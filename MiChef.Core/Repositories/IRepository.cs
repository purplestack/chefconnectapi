﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Core.Repositories
{
    public interface IRepository<TEntity>
     where TEntity : class
    {
        void Update(TEntity Entity);
        void DetachAndUpdate(int id, TEntity modifiedEntity);
        IList<TEntity> Filter(Expression<Func<TEntity, bool>> predicate);
        TEntity Get(int id);
        IQueryable<TEntity> GetAll();
        void Save(TEntity entity);
    }
}
