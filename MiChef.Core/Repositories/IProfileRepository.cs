﻿using ChefConnect.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Core.Repositories
{
    public interface IProfileRepository : IRepository<Profile>
    {

    }
}
