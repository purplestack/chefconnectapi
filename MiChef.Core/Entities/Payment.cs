﻿using ChefConnect.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Core.Entities
{
    public class Payment : Object
    {
        public string ErrorMsg;
        public string PaymentRef { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public PaymentStatus Status { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public decimal Tax { get; set; }
        public decimal Commission { get; set; }
        public decimal Net { get; set; }
        public string ToAccountName { get; set; }
        public string ToAccountNumber { get; set; }
        public string ToBankName { get; set; }
        public string StripeChargeID { get; set; }
    }

    public class OutstandignPayment : Object
    {
        public string PaymentRef { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public DateTime DateOpened { get; set; }
        public DateTime? DateClosed { get; set; }
        public int ClientProfileID { get; set; }
        public string ListOfDaysJson { get; set; }
        public Dictionary<DateTime, List<Session>> ListOfDays { get; set; }
        public decimal RunningTotal { get; set; }
        public decimal HoursServiced { get; set; }
    }
}
