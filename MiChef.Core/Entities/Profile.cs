﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Core.Entities
{
    public class Profile : Object
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string LastLoginDate { get; set; }
        public string RegistrationDate { get; set; }
        public string WorkPlace { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string ProfilePicture { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public string Location { get; set; }
        public ProfileType ProfileType { get; set; }
        [NotMapped]
        public double Distance { get; set; }
        public List<int> Favourites { get; set; }
        public bool isThirdPartyProfile { get; set; }
        public string PaymentToken { get; set; }
        public string FireBaseToken { get; set; }
        public string NationalInsurance { get; set; }
        public string UTR { get; set; }
        public bool isSelfEmployed { get; set; }
        public string secureOTP { get; set; }
    }

    public enum ProfileType
    {
        User = 0,
        Chef = 1
    }
}
