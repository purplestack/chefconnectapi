﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Core.Entities.Common
{
    public enum JobStatus
    {
        Pending = 0,
        Approved = 1,
        Session_Started = 2,
        Session_Completed = 3,
        Declined = 4,
        Cancelled = 5
    }

    public enum SessionStatus
    {
        Pending = 0,
        Canceled = 1,
        Confirmed = 2,
        Ongoing = 3,
        Ended = 4
    }

    public enum PaymentStatus
    {
        PendingPayment = 0,
        Paid = 1
    }
}
