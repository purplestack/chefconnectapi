﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Core.Entities
{
    public class Chef : Object
    {
        public string DateOfBirth { get; set; }
        public bool IsVerified { get; set; }
        public int Rating { get; set; }
        public decimal Rate { get; set; }
        public decimal BaseRate { get; set; }
        public string KitchenExperience { get; set; } // Pastery, Main Kitchen, Michelin Star experience
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }
        [NotMapped]
        public double Distance { get; set; }
        public string Passport { get; set; }
        public string Resume { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string BankName { get; set; }
    }
}
