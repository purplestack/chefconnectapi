﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Core.Entities
{
    public class User:Object
    {
        public string CardPAN { get; set; }
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }
    }
}
