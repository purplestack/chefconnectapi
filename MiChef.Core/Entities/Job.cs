﻿using ChefConnect.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Core.Entities
{
    public class Job : Object
    {
        public string JobDescription { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartTime { get; set; }
        public int ChefId { get; set; }
        public int ProfileId { get; set; }
        public int JobRating { get; set; }
        public JobStatus JobStatus { get; set; }
        [NotMapped]
        public string DisplayName { get; set; }
        [NotMapped]
        public string Location { get; set; }
        [NotMapped]
        public decimal Rate { get; set; }
        [NotMapped]
        public double Distance { get; set; }
    }


    public class Session : Object
    {
        public int ClientProfileId { get; set; }
        public int ChefId { get; set; }
        public DateTime Date { get; set; }
        public DateTime TimeIn { get; set; }
        public DateTime TimeOut { get; set; }
        public SessionStatus SessionStatus { get; set; }
        public string Review { get; set; }
        public int Rating { get; set; }
        public int JobId { get; set; }
        public Job Job { get; set; }
        [NotMapped]
        public DateTime CurrentTime { get; set; }
        [NotMapped]
        public string DisplayName { get; set; }
        public bool StartTimeModified { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal HoursWorked { get; set; }
    }
}
