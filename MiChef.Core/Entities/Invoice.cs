﻿using ChefConnect.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Core.Entities
{
    public class Invoice : Object
    {
        public string InvoiceRef { get; set; }
        public PaymentStatus InvoiceStatus { get; set; }
        public DateTime DateOpened { get; set; }
        public DateTime? DateClosed { get; set; }
        public int ChefId { get; set; }
        public string ListOfDaysJson { get; set; }
        public Dictionary<DateTime, List<Session>> ListOfDays { get; set; }
        public decimal RunningTotal { get; set; }
        public decimal HoursWorked { get; set; }

    }
}
