﻿using ChefConnect.Core.Repositories;
using ChefConnect.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Persistence
{
    public static class RepositoryFactory
    {
        public static IChefRepository GetChefRepository()
        {
            return new ChefRepository();
        }

        public static IUserRepository GetUserRepository()
        {
            return new UserRepository();
        }
        public static IProfileRepository GetProfileRepository()
        {
            return new ProfileRepository();
        }

        public static IJobRepository GetJobRepository()
        {
            return new JobRepository();
        }

        public static ISessionRepository GetSessionRepository()
        {
            return new SessionRepository();
        }

        public static IPaymentRepository GetPaymentRepository()
        {
            return new PaymentRepository();
        }

        public static IInvoiceRepository GetInvoiceRepository()
        {
            return new InvoiceRepository();
        }

        public static IOutstandingPaymentRepository GetOutstandingPaymentRepository()
        {
            return new OutstandingPaymentRepository();
        }
    }
}
