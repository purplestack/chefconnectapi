﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ChefConnect.Core.Entities;
using ChefConnect.Persistence.Migrations;

namespace ChefConnect.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("ChefConnect")
        {
            this.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Profile>().Property(a => a.Latitude).HasPrecision(18, 6);
            modelBuilder.Entity<Profile>().Property(a => a.Longitude).HasPrecision(18, 6);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


        public DbSet<Chef> Chef_Connect_Chefs { get; set; }
        public DbSet<User> Chef_Connect_Users { get; set; }
        public DbSet<Profile> Chef_Connect_Profiles { get; set; }
        public DbSet<Job> Chef_Connect_Jobs { get; set; }
        public DbSet<Session> Chef_Connect_Session { get; set; }
        public DbSet<Payment> Chef_Connect_Payment { get; set; }
        public DbSet<Invoice> Chef_Connect_Invoice { get; set; }
        public DbSet<OutstandignPayment> Chef_Connect_OutstandingPaymnets{ get; set; }

    }
}
