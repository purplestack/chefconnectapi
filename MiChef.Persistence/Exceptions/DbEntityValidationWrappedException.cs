﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Persistence.Exceptions
{
    public class EntityError
    {
        public string Property { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class DbEntityValidationWrappedException : Exception
    {
        private IDictionary<string, List<EntityError>> _entityErrors;

        public DbEntityValidationWrappedException(DbEntityValidationException dbex) : base("Db Entity Validation Error")
        {
            _entityErrors = new Dictionary<string, List<EntityError>>();

            foreach (var e in dbex.EntityValidationErrors)
            {
                var entityName = e.Entry.Entity.GetType().Name;

                if (!_entityErrors.ContainsKey(entityName))
                {
                    _entityErrors[entityName] = new List<EntityError>();
                }

                foreach (var prop in e.ValidationErrors)
                {
                    _entityErrors[entityName].Add(new EntityError
                    {
                        Property = prop.PropertyName,
                        ErrorMessage = prop.ErrorMessage
                    });
                }
            }
        }

        public override string ToString() => JsonConvert.SerializeObject(_entityErrors);
    }
}
