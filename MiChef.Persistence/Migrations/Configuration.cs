﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Persistence.Migrations
{
    public class Configuration : DbMigrationsConfiguration<ChefConnect.Persistence.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "MiChef.Persistence.ApplicationDbContext";
        }

        protected override void Seed(ChefConnect.Persistence.ApplicationDbContext context)
        {

        }
    }
}
