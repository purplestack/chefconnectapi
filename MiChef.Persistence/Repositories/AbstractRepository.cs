﻿using ChefConnect.Persistence.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Persistence.Repositories
{
    internal abstract class AbstractRepository<TEntity, TContext>
        where TEntity : class
        where TContext : ApplicationDbContext, new()
    {
        protected TContext DataContext { get { return new TContext(); } }

        public TEntity Get(int SearchKey)
        {
            var Context = DataContext;
            return Context.Set<TEntity>().Find(SearchKey);
        }

        public void Save(TEntity Entity)
        {
            var context = DataContext;
            try
            {
                context.Entry<TEntity>(Entity).State = EntityState.Added;
                context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw new DbEntityValidationWrappedException(dbEx);
            }
        }

        public void Update(TEntity entity)
        {
            var context = DataContext;
            try
            {
                context.Entry<TEntity>(entity).State = EntityState.Modified;
                context.SaveChanges();
            }

            catch (DbEntityValidationException dbex)
            {
                throw new DbEntityValidationWrappedException(dbex);
            }
        }

        public void DetachAndUpdate(int id, TEntity modifiedEntity)
        {
            var entity = Get(id);

            var context = DataContext;
            try
            {
                context.Entry(modifiedEntity).State = EntityState.Modified;
                context.SaveChanges();
            }
            catch (DbEntityValidationException dbex)
            {
                throw new DbEntityValidationWrappedException(dbex);
            }

        }

        public IList<TEntity> Filter(Expression<Func<TEntity, bool>> predicate)
        {
            var context = DataContext;
            return context.Set<TEntity>().Where(predicate).ToList();
        }

        public IQueryable<TEntity> GetAll()
        {
            var context = DataContext;           
            return context.Set<TEntity>();
        }
    }
}
