﻿using ChefConnect.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheChefConnect.Services;
using TheChefConnect.Services.ServiceEntites;

namespace ChefConnect.Api.Controllers
{
    public class OutstandingPaymentController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private OutstandingPaymentService outstandingPaymentService = new OutstandingPaymentService();

        // GET: Invoice
        [Route("GetOutstandingPayment")]
        public async Task<IHttpActionResult> GetRunningInvoice([FromUri]GetOutstandignPaymentRequest request)
        {
            GetOutstandingPaymentResponse response = await outstandingPaymentService.GetCurrentOutstandingPayment(request);
            return Ok(response);
        }


        [Route("SendOutstandingPaymentToMail")]
        public async Task<IHttpActionResult> SendOutstandingPaymentToMail([FromUri]GetOutstandignPaymentRequest request)
        {
            OutstandingPaymentResponse response = await outstandingPaymentService.SendOutstandingPaymentToMail(request);
            return Ok(response);
        }
    }
}