﻿using ChefConnect.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChefConnect.Services;
using ChefConnect.Services.ServiceEntites;

namespace ChefConnect.Api.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private UserService userService = new UserService();

        // GET: api/Chefs
        [Route("Users")]
        [ResponseType(typeof(UserListResponse))]
        public IHttpActionResult users(UserRequest request)
        {
            UserListResponse user = userService.RetrieveAll(request);
            return Ok(user);
        }

        [Route("RegisterUser")]
        [ResponseType(typeof(UserResponse))]
        public async Task<IHttpActionResult> RegisterUser(User user)
        {
            var registeredUser = await userService.RegisterUser(user);
            return Ok(registeredUser);
        }

        // GET: api/Chefs/5
        [Route("Get")]
        [ResponseType(typeof(UserResponse))]
        public IHttpActionResult Get(int id)
        {
            UserResponse response = userService.FindUser(id);
            return Ok(response);
        }

        [Route("Put")]
        // PUT: api/Chefs/5
        [ResponseType(typeof(UserResponse))]
        public async Task<IHttpActionResult> Put(User user)
        {
            UserResponse response = null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                response = await userService.UpdateUser(user);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(user.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(response);
        }


        private bool UserExists(int id)
        {
            return userService.FindUser(id) != null;
        }
    }
}
