﻿using ChefConnect.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TheChefConnect.Services;
using TheChefConnect.Services.ServiceEntites;

namespace ChefConnect.Api.Controllers
{
    public class InvoiceController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private InvoiceService invoiceService = new InvoiceService();

        // GET: Invoice
        [Route("GetRunningInvoice")]
        public async Task<IHttpActionResult> GetRunningInvoice([FromUri]GetInvoiceRequest request)
        {
            GetInvoiceResponse response = await invoiceService.GetCurrentInvoice(request);
            return Ok(response);
        }

        [Route("SendInvoiceToMail")]
        public async Task<IHttpActionResult> SendInvoiceToMail([FromUri]GetInvoiceRequest request)
        {
            InvoiceResponse response = await invoiceService.SendInvoiceToMail(request);
            return Ok(response);
        }
    }
}