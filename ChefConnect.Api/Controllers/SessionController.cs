﻿using ChefConnect.Core.Entities;
using ChefConnect.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheChefConnect.Services;
using TheChefConnect.Services.ServiceEntites;

namespace ChefConnect.Api.Controllers
{
    [RoutePrefix("api/Session")]
    public class SessionController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private SessionService sessionService = new SessionService();

        [Route("CreateSession")]
        public async Task<IHttpActionResult> CreateSession([FromBody]Session request)
        {
            SessionResponse response = await sessionService.CreateSession(request);
            return Ok(response);
        }

        [Route("ClockSession")]
        public async Task<IHttpActionResult> ClockSession([FromBody]ClockSessionRequest request)
        {
            SessionResponse response = await sessionService.ClockSession(request);
            return Ok(response);
        }

        [Route("UpdateSessionStatus")]
        public async Task<IHttpActionResult> UpdateSessionStatus([FromBody]UpdateSessionRequest request)
        {
            SessionResponse response = await sessionService.UpdateSessionStatus(request);
            return Ok(response);
        }

        [Route("GetSession")]
        public async Task<IHttpActionResult> GetSession([FromUri]SessionRequest request)
        {
            SessionResponse response = await sessionService.FindSession(request.SessionId);
            return Ok(response);
        }

        [Route("GetAllSessions")]
        public async Task<IHttpActionResult> GetAllSessions([FromUri]SessionByJobRequest request)
        {
            AllSessionResponse response = await sessionService.GetAllSession(request.JobId);
            return Ok(response);
        }

        [Route("GetCurrentSessions")]
        public async Task<IHttpActionResult> GetCurrentSessions([FromUri]CurrentSessionRequest request)
        {
            CurrentSessions response = await sessionService.GetCurrentSessions(request);
            return Ok(response);
        }

        [Route("GetRequestedSessions")]
        public async Task<IHttpActionResult> GetRequestedSessions([FromUri]GetRequestedSession request)
        {
            CurrentSessions response = await sessionService.GetRequestedSessions(request);
            return Ok(response);
        }

        [Route("GetReceivedSessions")]
        public async Task<IHttpActionResult> GetReceivedSessions([FromUri]GetReceivedSession request)
        {
            CurrentSessions response = await sessionService.GetReceivedSessions(request);
            return Ok(response);
        }

        [Route("UpdateRatingAndReview")]
        public async Task<IHttpActionResult> UpdateRatingAndReview([FromUri]UpdateSessionRating request)
        {
            SessionResponse response = await sessionService.UpdateRatingAndReview(request);
            return Ok(response);
        }

        [Route("UpdateStartTime")]
        public async Task<IHttpActionResult> UpdateStartTime([FromUri]UpdateSessionTimeRequest request)
        {
            SessionResponse response = await sessionService.UpdateStartTime(request);
            return Ok(response);
        }
    }
}
