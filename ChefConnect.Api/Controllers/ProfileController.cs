﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChefConnect.Services;
using ChefConnect.Services.ServiceEntites;
using System.Drawing;
using TheChefConnect.Services;
using TheChefConnect.Services.ServiceEntites;
using ChefConnect.Core.Entities;

namespace ChefConnect.Api.Controllers
{
    [RoutePrefix("api/Profile")]
    public class ProfileController : ApiController
    {
        private ProfileService service = new ProfileService();

        private PaymentService _paymentService = new PaymentService();

        [Route("UserLogin")]
        public async Task<IHttpActionResult> UserLogin([FromBody]LoginRequest request)
        {
            LoginResponse chefs = await service.UserLogin(request);
            return Ok(chefs);
        }

        [Route("UpdateProfile")]
        public async Task<IHttpActionResult> UpdateProfile([FromBody] Profile Request)
        {
            ProfileResponse response = await service.UpdateProfile(Request);
            return Ok(response);
        }

        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword([FromBody]ChangePasswordRequest request)
        {
            ProfileResponse chefs = await service.ChangePassword(request);
            return Ok(chefs);
        }

        [Route("ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword([FromBody]ForgotPasswordRequest request)
        {
            ProfileResponse chefs = await service.ForgotPassword(request);
            return Ok(chefs);
        }

        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword([FromBody]ResetPasswordRequest request)
        {
            ProfileResponse chefs = await service.ResetPassword(request);
            return Ok(chefs);
        }

        [Route("GetProfilePicture")]
        public async Task<IHttpActionResult> GetProfilePicture([FromUri]ProfilePictureRequest request)
        {
            ProfilePictureResponse chefs = await service.GetProfilePicture(request);
            return Ok(chefs);
        }

        [HttpPost]
        [Route("UpdateProfilePicture")]
        public async Task<IHttpActionResult> UpdateProfilePicture([FromBody]UpdatePictureRequest request)
        {
            ProfileResponse response = await service.UpdateProfilePicture(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("UpdateLocation")]
        public async Task<IHttpActionResult> UpdateLocation([FromBody]UpdateLocationRequest request)
        {
            ProfileResponse response = await service.UpdateLocation(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("CreatePaymentToken")]
        public async Task<IHttpActionResult> CreatePaymentToken([FromBody]CreatePaymentToken request)
        {
            ProfileResponse response = await service.CreatePaymentToken(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("UpdatePaymentToken")]
        public async Task<IHttpActionResult> UpdatePaymentToken([FromBody]UpdatePaymentToken request)
        {
            ProfileResponse response = await service.UpdatePaymentToken(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("UpdateFireBaseToken")]
        public async Task<IHttpActionResult> UpdateFireBaseToken([FromBody]UpdateFireBaseToken request)
        {
            ProfileResponse response = await service.UpdateFireBaseToken(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("GetCardDetails")]
        public async Task<IHttpActionResult> GetCardDetails([FromUri]string StripeCustomerToken)
        {
            CustomerCard response = await PaymentService.RetrieveCustomerCard(StripeCustomerToken);
            return Ok(response);
        }

        [HttpPost]
        [Route("UpdateTaxDetails")]
        public async Task<IHttpActionResult> UpdateTaxDetails([FromUri]UpdateTaxDetails request)
        {
            ProfileResponse response = await service.UpdateTaxDetails(request);
            return Ok(response);
        }
    }
}
