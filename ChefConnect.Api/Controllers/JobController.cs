﻿using ChefConnect.Core.Entities;
using ChefConnect.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ChefConnect.Services;
using ChefConnect.Services.ServiceEntites;

namespace ChefConnect.Api.Controllers
{
    [RoutePrefix("api/Job")]
    public class JobController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private JobService jobService = new JobService();

        [Route("RequestJob")]
        public async Task<IHttpActionResult> RequestJob([FromUri]Job request)
        {
            JobResponse response = await jobService.RequestJob(request);
            return Ok(response);
        }

        [Route("UpdateJobStatus")]
        public async Task<IHttpActionResult> UpdateJobStatus([FromUri]UpdateJobStatus request)
        {
            JobResponse response = await jobService.UpdateJobStatus(request);
            return Ok(response);
        }

        [Route("GetJob")]
        public async Task<IHttpActionResult> GetJob([FromUri]JobRequest request)
        {
            JobResponse response = await jobService.FindJob(request.Id);
            return Ok(response);
        }

        [Route("GetAllSentJob")]
        public async Task<IHttpActionResult> GetAllSentJob([FromUri]GetAllSentJob request)
        {
            GetAllJobsResponse response = await jobService.GetAllSentJobs(request);
            return Ok(response);
        }

        [Route("GetAllReceivedJob")]
        public async Task<IHttpActionResult> GetAllReceivedJob([FromUri]GetAllReceivedJob request)
        {
            GetAllJobsResponse response = await jobService.GetAllReceivedJobs(request);
            return Ok(response);
        }

        [Route("GetJobHistory")]
        public async Task<IHttpActionResult> GetJobHistory([FromUri]GetJobHistory request)
        {
            GetJobHistoryResponse response = await jobService.GetJobHistory(request);
            return Ok(response);
        }
    }
}
