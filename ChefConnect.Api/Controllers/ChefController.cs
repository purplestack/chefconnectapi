﻿using ChefConnect.Core.Entities;
using ChefConnect.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ChefConnect.Services;
using ChefConnect.Services.ServiceEntites;

namespace ChefConnect.Api.Controllers
{
    [RoutePrefix("api/Chef")]
    public class ChefController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ChefService chefService = new ChefService();

        [Route("GetChefs")]
        public IHttpActionResult GetChefs([FromUri]GetAllChefsRequest request)
        {
            ChefListResponse chefs = chefService.RetrieveAll(request);
            return Ok(chefs);
        }


        [HttpPost]
        [Route("GetChefCategories")]
        public IHttpActionResult GetChefCategories([FromBody]GetChefCategoriesRequest request)
        {
            ChefCategories response = chefService.RetrieveAllCategories(request);
            return Ok(response);
        }

        [HttpGet]
        [Route("GetChef")]
        public IHttpActionResult GetChef([FromUri]FindChefByID request)
        {
            ChefResponse response = null;

            response = chefService.FindChef(request.ID);
            return Ok(response);
        }

        [Route("RetrieveChefDetails")]
        public IHttpActionResult GetChefDetails([FromUri]FindChefByID request)
        {
            ChefResponse response = null;

            response = chefService.LoadFullDetails(request.ID);
            return Ok(response);
        }

        [Route("GetChefByEmail")]
        public async Task<IHttpActionResult> GetChefByEmail([FromUri]GetChefByEmail request)
        {
            ChefResponse response = null;
            response = await chefService.GetChefByEmail(request);
            return Ok(response);
        }

        [Route("GetChefByUsername")]
        public async Task<IHttpActionResult> GetChefByUserName([FromUri]GetChefByUsername request)
        {
            ChefListResponse response = null;
            response = await chefService.FindChefByUsername(request);
            return Ok(response);
        }

        [Route("GetChefByKitchenExperience")]
        public async Task<IHttpActionResult> GetChefByKitchenExperience([FromUri]GetChefByKitchenExperience request)
        {
            ChefListResponse response = null;
            response = await chefService.FindChefByKitchenExp(request);
            return Ok(response);
        }

        [Route("RegisterChef")]
        public async Task<IHttpActionResult> RegisterChef([FromBody]Chef chef)
        {
            ChefResponse response = await chefService.RegisterChef(chef);
            return Ok(response);
        }

        // PUT: api/Chefs/5
        [Route("UpdateChef")]
        [ResponseType(typeof(ChefResponse))]
        public async Task<IHttpActionResult> UpdateChef([FromBody]Chef chef)
        {
            ChefResponse response = null;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            response = await chefService.UpdateChef(chef);

            return Ok(response);
        }

        [Route("UpdateChefRate")]
        [ResponseType(typeof(ChefResponse))]
        public async Task<IHttpActionResult> UpdateChefRate([FromBody]UpdateRate request)
        {
            ChefResponse response = await chefService.UpdateRate(request);
            return Ok(response);
        }

        [Route("UpdateBankDetails")]
        [ResponseType(typeof(ChefResponse))]
        public async Task<IHttpActionResult> UpdateBankDetails([FromBody]UpdateBankDetails request)
        {
            ChefResponse response = await chefService.UpdateBankDetails(request);
            return Ok(response);
        }

        [Route("UpdateKitchenExperience")]
        [ResponseType(typeof(ChefResponse))]
        public async Task<IHttpActionResult> UpdateKitchenExperience([FromBody]UpdateKitchenExperience request)
        {
            ChefResponse response = await chefService.UpdateKitchenExperience(request);
            return Ok(response);
        }

        [Route("FindChefByKitchenExperience")]
        [ResponseType(typeof(ChefResponse))]
        public async Task<IHttpActionResult> FindChefByKitchenExperience([FromBody]GetChefByKitchenExperience request)
        {
            ChefListResponse response = await chefService.FindChefByKitchenExp(request);
            return Ok(response);
        }



        private bool ChefExists(int id)
        {
            return chefService.FindChef(id) != null;
        }
    }
}
