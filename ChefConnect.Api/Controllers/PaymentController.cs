﻿using ChefConnect.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TheChefConnect.Services;
using TheChefConnect.Services.ServiceEntites;

namespace ChefConnect.Api.Controllers
{
    [RoutePrefix("api/Payment")]
    public class PaymentController : ApiController
    {
        private PaymentService service = new PaymentService();

        [Route("GetPaymentDetails")]
        public async Task<IHttpActionResult> GetPaymentDetails([FromUri]int SessionID)
        {
            PaymentResponse payment = await service.GetPayment(SessionID);
            return Ok(payment);
        }

        [Route("GetPaymentByJobId")]
        public async Task<IHttpActionResult> GetPaymentByJobId([FromUri]GetPaymentByJobID request)
        {
            PaymentResponse payment = await service.GetPaymentByJobId(request);
            return Ok(payment);
        }

        [Route("GetAllInvoices")]
        public async Task<IHttpActionResult> GetAllInvoices([FromUri] GetInvoiceHistory request)
        {
            PaymentHistory invoices = await service.GetInvoiceHistory(request);
            return Ok(invoices);
        }

        [Route("GetAllPayments")]
        public async Task<IHttpActionResult> GetAllPayments([FromUri]GetPaymentHistory request)
        {
            PaymentHistory payments = await service.GetPaymentHistory(request);
            return Ok(payments);
        }
    }
}
