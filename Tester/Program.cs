﻿
using System;
using ChefConnect.Services;
using ChefConnect.Core.Entities;
using System.IO;
using TheChefConnect.Services;
using TheChefConnect.Services.ServiceEntites;
using OfficeOpenXml;
using System.Drawing;

namespace Tester
{
    public class Program
    {

        //IChefRepository _chefRepository;
        private static ChefService chefService = new ChefService();
        private static ProfileService profileService = new ProfileService();
        //public Program() : this(RepositoryFactory.GetChefRepository()) { }

        //public Program(IChefRepository chefRepository)
        //{
        //    _chefRepository = chefRepository;
        public static void Main(string[] args)
        {
            //Dictionary<string,string> userDetails= new Dictionary<string, string>();
            //userDetails.Add("ClientID", "1234-5678-9012");
            //userDetails.Add("InstitutionCode", "65");
            //userDetails.Add("Username", "bsosan");
            string dateFomrat = DateTime.Now.ToString("ddMMyyyyhhmmss");
            String path = @"C:\Users\RONNIE\Desktop\testsheet3.xlsx";
            //FileInfo newFile = null;
            /*if (!File.Exists(path + "\\testsheet2.xlsx"))
                newFile = new FileInfo(path + "\\testsheet2.xlsx");
            else
                return newFile;*/

            var newFile = new FileInfo(path);
            using (ExcelPackage package = new ExcelPackage())
            {
                Invoice invoice = new InvoiceService().GetCurrentInvoice(new GetInvoiceRequest { ChefID = 2031 }).Result.ChefInvoice;
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("testsheet");

                ws.Cells["B1"].Value = "Date";
                ws.Cells["C1"].Value = "Chef Name";
                ws.Cells["D1"].Value = "Hours Worked";
                ws.Cells["E1"].Value = "Income";
                ws.Cells["B1:E1"].Style.Font.Bold = true;
                ws.Cells["B1:E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.LightTrellis;
                ws.Cells["B1:E1"].Style.Fill.BackgroundColor.SetColor(Color.SkyBlue);

                int x = 2;
                char date = 'B';
                char chef_Name = 'C';
                char hours_Worked = 'D';
                char income = 'E';

                foreach (var day in invoice.ListOfDays)
                {
                    foreach (var session in day.Value)
                    {
                        ws.Cells[chef_Name + x.ToString()].Value = session.DisplayName;
                        ws.Cells[hours_Worked + x.ToString()].Value = session.HoursWorked;
                        ws.Cells[income + x.ToString()].Value = session.TotalAmount;
                    }
                    x++;
                }
                x++;

                ws.Cells["A" + x.ToString()].Value = "TOTAL";
                ws.Cells[hours_Worked + x.ToString()].Value = invoice.HoursWorked;
                ws.Cells[income + x.ToString()].Value = invoice.RunningTotal;
                ws.Cells[string.Format("{0}:{1}:{2}", "A" + x.ToString(), hours_Worked + x.ToString(), income + x.ToString())].Style.Font.Bold = true;
                //newFile.Create();
                //newFile.MoveTo(@"C:/testSheet.xlsx");
                //package.SaveAs(newFile);
                package.Save();

                //Create excel file on physical disk    
                FileStream objFileStrm = File.Create(path);
                objFileStrm.Close();

                //Write content to excel file    
                File.WriteAllBytes(path, package.GetAsByteArray());
            }
        }
    }

}
