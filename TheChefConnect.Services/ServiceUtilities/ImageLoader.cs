﻿using ChefConnect.Services.ServiceUtilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheChefConnect.Services.ServiceUtilities
{
    public class ImageLoader
    {
        public string ImageBaseURL = System.Configuration.ConfigurationManager.AppSettings.Get("ImageBaseURL");
        public Image ConvertImage(string ImageString)
        {
            //data:image/gif;base64,
            //this image is a single pixel (black)
            byte[] bytes = Convert.FromBase64String(ImageString);

            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }
            return image;
        }

        public void LoadImage(string ProfileID, out Image image)
        {
            //data:image/gif;base64,
            //this image is a single pixel (black)
            string fileLocation = string.Format("{0}{1}s\\{1}{2}{3}", AppDomain.CurrentDomain.BaseDirectory, "ProfilePicture", ProfileID, ".png");

            try
            {
                image = Image.FromFile(fileLocation);
            }
            catch (Exception ex)
            {
                image = null;
            }
        }

        public string SaveImage(string ProfileID, string base64String, string nameTag)
        {
            Logger.LogInfo("ImageLoader.SaveImage", nameTag + " " + ProfileID);
            byte[] bytes = Convert.FromBase64String(base64String);
            string FileName = string.Empty;
            string existingFileName = string.Empty;
            Image image;
            Image LoadedImage = null;
            bool isDone = false;
            string newNameTag = "ProfilePicture" + ProfileID + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png"; //using the naming convention for profile pictures
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
                //Generate file path suing existing filename for the purpose of searching for the existing file to be deleted
                existingFileName = GenerateFilePath(nameTag);
                //Generate file path using the new filename for the purpose of saving a new profile picture
                FileName = GenerateFilePath(newNameTag);
                try
                {
                    Logger.LogInfo("Trying to Load initial Image", existingFileName);
                    LoadedImage = Image.FromFile(existingFileName);
                }
                catch (Exception ex)
                {
                    Logger.LogInfo("ImageLoad unsuccessful for existing file name", "..Attempting new file name");
                    try
                    {

                        LoadedImage = Image.FromFile(FileName);
                    }
                    catch (Exception exx)
                    {
                        Logger.LogInfo("ImageLoad unsuccessful for new file name", "..  Attempting to save new file");
                        if (LoadedImage != null)
                        {
                            LoadedImage.Dispose();
                        }
                        image.Save(FileName);
                        isDone = true;
                        Logger.LogInfo("saveImage", string.Format("ImageUploaded: {0}", FileName));
                    }
                }

                if (LoadedImage != null && isDone == false)
                {
                    LoadedImage.Dispose();
                    File.Delete(existingFileName);
                    image.Save(FileName);
                }
            }
            FileName = string.Format("{0}/{1}", ImageBaseURL, newNameTag);
            Logger.LogInfo("ImageURL", FileName);
            return FileName;
        }

        private string GenerateFilePath(string FileName)
        {
            string FileLocation = string.Format("{0}{1}s\\{2}", AppDomain.CurrentDomain.BaseDirectory, "ProfilePicture", FileName);
            Logger.LogInfo("ImageLoader,GenerateFilePath", FileLocation);
            return FileLocation;
        }
    }
}
