﻿using ChefConnect.Services.ServiceUtilities;
using net.tipstrade.FCMNet.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
namespace TheChefConnect.Services.ServiceUtilities
{
    public class FirebaseProcessor<G, T>
    {
        DataExchangeFormat _dataFormat = DataExchangeFormat.JSON;
        string FireBaseToken = System.Configuration.ConfigurationManager.AppSettings.Get("FireBaseToken");
        string _apiEndPoint = string.Empty;

        public FirebaseProcessor(DataExchangeFormat dataFormat = DataExchangeFormat.JSON)
        {
            _apiEndPoint = System.Configuration.ConfigurationManager.AppSettings.Get("FireBaseUrl"); ;
            _dataFormat = dataFormat;
        }

        public object DoPOST(G RequestMessage)
        {
            Logger.LogInfo("FirebaseProcessor.DoPOST: request", RequestMessage);
            T retVal = default(T);

            try
            {
                StringBuilder requestPayload = new StringBuilder();
                using (var writer = new StringWriter(requestPayload))
                {
                    new Newtonsoft.Json.JsonSerializer().Serialize(writer, RequestMessage, typeof(G));
                }
                Logger.LogInfo("FirebaseProcessor.DoPOST", String.Format("Request Payload: {0}", requestPayload.ToString()));

                string mediaType = GetMediaType();
                string rawResponse = string.Empty;
                using (HttpClient client = new HttpClient())
                {
                    HttpRequestMessage request = new HttpRequestMessage() { RequestUri = new Uri(_apiEndPoint), Method = HttpMethod.Post, Content = new StringContent(requestPayload.ToString(), Encoding.UTF8, mediaType) };
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));
                    request.Headers.TryAddWithoutValidation("Authorization", FireBaseToken);

                    HttpResponseMessage response = client.SendAsync(request).Result;
                    response.EnsureSuccessStatusCode();
                    rawResponse = response.Content.ReadAsStringAsync().Result;
                }
                Logger.LogInfo("FirebaseProcessor.DoPOST: rawResponse", rawResponse);
                retVal = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(rawResponse);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return retVal;
        }

        public string GetMediaType()
        {
            string retVal = string.Empty;
            switch (_dataFormat)
            {
                case DataExchangeFormat.JSON:
                    retVal = "application/json";
                    break;
                case DataExchangeFormat.XML:
                    retVal = "text/xml";
                    break;
                default:
                    throw new ApplicationException($"Data Exchange Format [{_dataFormat}] is currently not supported.");
            }
            return retVal;
        }

        public enum DataExchangeFormat
        {
            JSON = 301,
            XML = 302,
        }
    }
}
