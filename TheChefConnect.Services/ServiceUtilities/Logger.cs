﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Services.ServiceUtilities
{
    public class Logger
    {
        static string filename = string.Format("{0}Logs\\Chef_Connect_Logs_{1:dd-MMM-yyyy}.txt", AppDomain.CurrentDomain.BaseDirectory, DateTime.Now);
        static bool enableRemoteLogger = false;//Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableRemoteLogger"]);
        static string LoggerUrl = "ww.ggg";//System.Configuration.ConfigurationManager.AppSettings["LoggerUrl"];
        public Logger()
        {

        }
        public static void LogInfo(string MethodName, object message)
        {
            message = message.GetType() != typeof(string) ? Newtonsoft.Json.JsonConvert.SerializeObject(message) : message;
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"));
                sb.AppendLine("caller: " + MethodName + "\n: " + message);

                using (System.IO.StreamWriter str = new System.IO.StreamWriter(filename, true))
                {
                    str.WriteLine(sb.ToString());
                }

                //Task.Factory.StartNew(() => ReportErrorToServer(sb.ToString()));
            }
            catch { }
        }

        public static void LogError(Exception ex)
        {
            try
            {
                string message = GetExceptionMessages(ex);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Date and Time: " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss"));
                sb.AppendLine("ErrorMessage: \n " + message);

                using (System.IO.StreamWriter str = new System.IO.StreamWriter(filename, true))
                {
                    str.WriteLine(sb.ToString());
                }

                Task.Factory.StartNew(() => ReportErrorToServer(sb.ToString()));
            }
            catch { }
        }

        public static string GetExceptionMessages(Exception ex)
        {
            string ret = string.Empty;
            if (ex != null)
            {
                ret = ex.Message;
                if (ex.InnerException != null)
                    ret = ret + "\n" + GetExceptionMessages(ex.InnerException);
            }
            return ret;
        }

        public static async void ReportErrorToServer(string msg)
        {
            try
            {
                if (enableRemoteLogger == false) return;

                HttpContent content = new StringContent(msg);

                using (HttpClient http = new HttpClient())
                {
                    //  HttpClient http = new HttpClient();
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, LoggerUrl);
                    req.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    req.Content = content;
                    req.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

                    var resp = await http.SendAsync(req);
                    var responseContent = await resp.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                using (System.IO.StreamWriter str = new System.IO.StreamWriter(filename, true))
                {
                    str.WriteLine(ex.ToString());
                }
            }

        }
    }
}
