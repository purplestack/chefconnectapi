﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Repositories;
using ChefConnect.Persistence;
using ChefConnect.Services;
using ChefConnect.Services.ServiceUtilities;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheChefConnect.Services.ServiceEntites;

namespace TheChefConnect.Services
{
    public class PaymentService
    {
        private IPaymentRepository _paymentRepository;
        private ApplicationDbContext dbcontext = new ApplicationDbContext();
        private static string _apiKey = System.Configuration.ConfigurationManager.AppSettings.Get("StripeApiKey");
        private TimeZoneInfo britishZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
        private PaymentService(IPaymentRepository PaymentRepository)
        {
            _paymentRepository = PaymentRepository;
        }

        public PaymentService() : this(RepositoryFactory.GetPaymentRepository())
        {
        }

        public PaymentResponse FindPayment(int Key)
        {
            Logger.LogInfo("ProfileService.FindUser UserID", Key.ToString());
            Payment payment = new Payment();
            PaymentResponse response = null;
            try
            {
                payment = dbcontext.Chef_Connect_Payment.Include("Session").Where(x => x.Id == Key).FirstOrDefault();

                response = new PaymentResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL",
                    Payment = payment,
                };
                if (payment == null)
                {
                    response = new PaymentResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Unable to process request",
                        Payment = payment,
                    };
                }
            }
            catch (Exception ex)
            {
                response = new PaymentResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                };
            }
            return response;
        }

        public Task<PaymentResponse> GetPayment(int SessionID)
        {
            Logger.LogInfo("ProfileService.FindUser UserID", SessionID.ToString());
            Payment payment = new Payment();
            PaymentResponse response = null;
            try
            {
                payment = dbcontext.Chef_Connect_Payment.Include("Session").Where(x => x.SessionId == SessionID).FirstOrDefault();

                response = new PaymentResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL",
                    Payment = payment,
                };
                if (payment == null)
                {
                    response = new PaymentResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Unable to process request",
                        Payment = payment,
                    };
                }
            }
            catch (Exception ex)
            {
                response = new PaymentResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<PaymentResponse> GetPaymentByJobId(GetPaymentByJobID request)
        {
            Logger.LogInfo("ProfileService.FindUser UserID", request);
            Payment payment = new Payment();
            PaymentResponse response = null;
            try
            {
                payment = dbcontext.Chef_Connect_Payment.Include("Session").Where(x => x.Session.JobId == request.JobID).FirstOrDefault();

                response = new PaymentResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL",
                    Payment = payment,
                };
                if (payment == null)
                {
                    response = new PaymentResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Unable to process request",
                        Payment = payment,
                    };
                }
            }
            catch (Exception ex)
            {
                response = new PaymentResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<PaymentResponse> CreatePayment(Payment request)
        {
            PaymentResponse response = null;
            try
            {
                request.PaymentDate = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);

                _paymentRepository.Save(request);

                Payment payment = FindPayment(request.Id).Payment;
                if (payment != null)
                {
                    response = new PaymentResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        Payment = payment,
                    };
                }
                else
                {
                    response = new PaymentResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Failed to create payment",
                        Payment = payment,
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new PaymentResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request",
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public string ChargeCustomer(string customerId, decimal amount, out string errorMsg)
        {
            errorMsg = string.Empty;
            try
            {
                StripeConfiguration.SetApiKey(_apiKey);
                var myCharge = new StripeChargeCreateOptions
                {
                    Amount = (int)amount,
                    Currency = "gbp",
                    Description = "Charge from Chef connect",
                    CustomerId = customerId,
                };

                var chargeService = new StripeChargeService();
                var stripeCharge = chargeService.Create(myCharge);

                return stripeCharge.Id;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                return null;
            }
        }

        public static string CreateCustomer(string tokenId, string email)
        {
            try
            {
                StripeConfiguration.SetApiKey(_apiKey);
                var token = tokenId; // Using ASP.NET MVC

                var customers = new StripeCustomerService(_apiKey);

                var customer = customers.Create(new StripeCustomerCreateOptions
                {
                    Email = email,
                    SourceToken = token

                });
                return customer.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static async Task<CustomerCard> RetrieveCustomerCard(string customerId)
        {
            CustomerCard card = new CustomerCard();
            try
            {
                return await System.Threading.Tasks.Task.Run(() =>
                   {
                       var customerService = new StripeCustomerService(_apiKey);

                       var customer = customerService.Get(customerId, null);
                       StripeCard customerCard = customer.Sources.FirstOrDefault().Card;
                       card = new CustomerCard
                       {
                           CardScheme = customerCard.Brand,
                           Country = customerCard.Country,
                           LastFourDigit = customerCard.Last4,
                           ResponseCode = "00",
                           ResponseDescription = "SUCCESSFUL"
                       };
                       return card;
                   });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return await Task.Factory.StartNew(() => card = new CustomerCard
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message
                });
            }
        }

        public static string UpdateCustomer(string customerId, string tokenId)
        {
            try
            {
                Logger.LogInfo("PaymentService.UpdateCustomer, input", string.Format("Customer:{0}, Token:{1}", customerId, tokenId));

                StripeConfiguration.SetApiKey(_apiKey);
                var token = tokenId; // Using ASP.NET MVC

                var customers = new StripeCustomerService(_apiKey);

                StripeCustomer verifyingCustomer = customers.Get(customerId);

                var customer = customers.Update(customerId, new StripeCustomerUpdateOptions
                {
                    SourceToken = token
                });
                return customer.Id;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Task<PaymentHistory> GetPaymentHistory(GetPaymentHistory request)
        {
            IQueryable<Payment> paymentList = null;
            PaymentHistory response = null;
            List<Payment> paymentFullDetails = new List<Payment>();
            try
            {
                paymentList = _paymentRepository.GetAll().Where(x => x.Session.ChefId == request.ChefID);
                if (paymentList == null || paymentList.Count() != 0)
                {
                    foreach (var item in paymentList)
                    {
                        paymentFullDetails.Add(FindPayment(item.Id).Payment);
                    }
                    PagedList<Payment> pagedPayments = new PagedList<Payment>(paymentFullDetails.AsQueryable().OrderBy(x => x.Id), request.CurrentPage, request.PageSize);
                    response = new PaymentHistory
                    {
                        Payments = pagedPayments,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new PaymentHistory
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No payments found"
                    };
                }
            }
            catch (Exception ex)
            {
                response = new PaymentHistory
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                    Payments = null
                };
            }

            return Task.Factory.StartNew(() => response);
        }


        public Task<PaymentHistory> GetInvoiceHistory(GetInvoiceHistory request)
        {
            IQueryable<Payment> paymentList = null;
            PaymentHistory response = null;
            List<Payment> paymentFullDetails = new List<Payment>();
            try
            {
                paymentList = _paymentRepository.GetAll().Where(x => x.Session.ClientProfileId == request.ClientID);
                if (paymentList == null || paymentList.Count() != 0)
                {
                    foreach (var item in paymentList)
                    {
                        paymentFullDetails.Add(FindPayment(item.Id).Payment);
                    }
                    PagedList<Payment> pagedPayments = new PagedList<Payment>(paymentFullDetails.AsQueryable().OrderBy(x => x.Id), request.CurrentPage, request.PageSize);
                    response = new PaymentHistory
                    {
                        Payments = pagedPayments,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new PaymentHistory
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No invoices found"
                    };
                }
            }
            catch (Exception ex)
            {
                response = new PaymentHistory
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                    Payments = null
                };
            }

            return Task.Factory.StartNew(() => response);
        }
        //public Boolean IsValidUTR(string UTR)
        //{
        //    throw NotImplementedException;
        //}

    }
}
