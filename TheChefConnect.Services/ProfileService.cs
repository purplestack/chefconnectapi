﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Repositories;
using ChefConnect.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChefConnect.Services.ServiceEntites;
using ChefConnect.Services.ServiceUtilities;
using System.Data.Entity;
using TheChefConnect.Services.ServiceUtilities;
using System.Drawing;
using TheChefConnect.Services;
using ChefConnect.Services.Extensions;
using TheChefConnect.Services.ServiceEntites;
using TheChefConnect.Services.NotificationService;

namespace ChefConnect.Services
{
    public class ProfileService
    {
        private IProfileRepository _profileRepository;
        private ApplicationDbContext dbcontext = new ApplicationDbContext();
        private ProfileService(IProfileRepository ProfileRepository)
        {
            _profileRepository = ProfileRepository;
        }

        public ProfileService() : this(RepositoryFactory.GetProfileRepository())
        {
        }

        public ProfileResponse FindProfile(int Key)
        {
            Logger.LogInfo("ProfileService.FindUser UserID", Key.ToString());
            Profile profile = new Profile();
            ProfileResponse response = null;
            try
            {
                profile = _profileRepository.Get(Key);

                if (profile == null)
                {
                    response = new ProfileResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Profile not found"
                    };
                }
                else
                {
                    response = new ProfileResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        Profile = profile,
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ProfileResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "No Profile Found",
                };
            }
            return response;
        }

        public ProfileResponse GetByEmail(string Email)
        {
            Logger.LogInfo("ProfileService.FindUser UserID", Email.ToString());
            Profile profile = new Profile();
            ProfileResponse response = null;
            try
            {
                profile = _profileRepository.GetAll().Where(x => x.Email == Email).FirstOrDefault();

                response = new ProfileResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL",
                    Profile = profile,
                };
                if (profile == null)
                {
                    response = new ProfileResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Unable to process request",
                        Profile = profile,
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ProfileResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                };
            }
            return response;
        }

        public Task<LoginResponse> UserLogin(LoginRequest request)
        {
            LoginResponse response = null;
            Profile profile = null;
            try
            {
                profile = _profileRepository.GetAll().Where(x => x.Email == request.emailAddress).FirstOrDefault();

                if (profile != null)
                {
                    if (profile.isThirdPartyProfile)
                    {
                        response = new LoginResponse
                        {
                            ResponseCode = "00",
                            ResponseDescription = "SUCCESSFUL",
                        };
                    }
                    else
                    {
                        if (profile.Password == request.Password)
                        {
                            response = new LoginResponse
                            {
                                ResponseCode = "00",
                                ResponseDescription = "SUCCESSFUL",
                            };
                        }
                        else
                        {
                            response = new LoginResponse
                            {
                                ResponseCode = "06",
                                ResponseDescription = "Login Failed"
                            };
                        }
                    }
                }
                else
                {
                    response = new LoginResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Invalid Email"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new LoginResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request"
                }; ;
            }

            if (response.ResponseCode == "00")
            {
                response.ProfileType = profile.ProfileType;
                switch (profile.ProfileType)
                {
                    case ProfileType.User:
                        response.user = dbcontext.Chef_Connect_Users.Include("Profile").Where(x => x.ProfileId == profile.Id).FirstOrDefault();
                        break;
                    case ProfileType.Chef:
                        response.chef = dbcontext.Chef_Connect_Chefs.Include("Profile").Where(x => x.ProfileId == profile.Id).FirstOrDefault();
                        break;
                    default:
                        break;
                }
            }

            return Task.Factory.StartNew(() => response);
        }

        /// <summary>
        /// Sends OTP to users email address for password reset
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<ProfileResponse> ForgotPassword(ForgotPasswordRequest request)
        {
            try
            {
                string OTP = new PasswordStorage().generateOTP();
                Profile userProfile = GetByEmail(request.Email).Profile;
                if (userProfile != null)
                {
                    if (userProfile.isThirdPartyProfile)
                    {
                        return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "06", ResponseDescription = "cannot reset password on this profile" });
                    }
                    MailRequest mailRequest = new MailRequest
                    {
                        MailBody = OTP,
                        MailRecipient = userProfile.Email,
                        MailSubject = "CHEF CONNECT RESET PASSWORD OTP",
                    };

                    userProfile.secureOTP = OTP;
                    _profileRepository.Update(userProfile);

                    Task.Run(() => new EmailService().SendMail(mailRequest));
                    return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "00", ResponseDescription = "SUCCESSFUL" });
                }
                else
                {
                    return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "06", ResponseDescription = "Profile not found for this email" });
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "06", ResponseDescription = "FAILED" });
            }
        }

        /// <summary>
        /// Reset user password after validating OTP
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<ProfileResponse> ResetPassword(ResetPasswordRequest request)
        {
            try
            {
                Profile userProfile = GetByEmail(request.Email).Profile;


                if (request.OTP == userProfile.secureOTP)
                {
                    userProfile.Password = request.NewPassword;
                }
                else
                {
                    return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "06", ResponseDescription = "Invalid OTP" });
                }

                _profileRepository.Update(userProfile);
                return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "00", ResponseDescription = "SUCCESSFUL" });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "06", ResponseDescription = "Unable to reset password" });
            }
        }

        /// <summary>
        /// Reset user password after validating OTP
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<ProfileResponse> ChangePassword(ChangePasswordRequest request)
        {
            try
            {
                Profile userProfile = FindProfile(request.Id).Profile;

                if (!userProfile.isThirdPartyProfile)
                {
                    if (request.CurrentPassword == userProfile.Password)
                    {
                        userProfile.Password = request.NewPassword;
                    }
                    else
                    {
                        return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "06", ResponseDescription = "Invalid Password" });
                    }

                }
                else
                {
                    return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "06", ResponseDescription = "Cannot change password for his profile" });
                }

                _profileRepository.Update(userProfile);
                return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "00", ResponseDescription = "SUCCESSFUL" });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return Task.Factory.StartNew(() => new ProfileResponse { ResponseCode = "06", ResponseDescription = "Unable to change password" });
            }
        }

        public Task<ProfilePictureResponse> GetProfilePicture(ProfilePictureRequest request)
        {
            ProfilePictureResponse response = null;
            try
            {
                Profile profile = _profileRepository.Get(request.Id);
                if (profile != null || profile.ProfilePicture == null)
                {
                    response = new ProfilePictureResponse
                    {
                        ProfilePicture = profile.ProfilePicture,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESS"
                    };
                }
                else
                {
                    response = new ProfilePictureResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "User not found"
                    };
                }
            }
            catch (Exception)
            {
                response = new ProfilePictureResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Could not process request"
                }; ;
            }

            return Task.Factory.StartNew(() => response);
        }

        public Task<ProfileResponse> UpdateProfilePicture(UpdatePictureRequest request)
        {
            ProfileResponse response = null;
            string nametag = string.Empty;
            try
            {
                Profile profile = FindProfile(request.Id).Profile;
                if (profile.ProfilePicture != null && profile.ProfilePicture != string.Empty)
                    nametag = profile.ProfilePicture.Split(new Char[] { '/' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

                Logger.LogInfo("ProfilePicture.NameTag", nametag);
                string imageLocation = new ImageLoader().SaveImage(request.Id.ToString(), request.ProfilePicture, nametag);
                //new ImageLoader().LoadImage(request.Id.ToString(), out response);
                profile = new Profile()
                {
                    Id = request.Id,
                    ProfilePicture = imageLocation
                };

                dbcontext.Chef_Connect_Profiles.Attach(profile);
                dbcontext.Entry(profile).Property(x => x.ProfilePicture).IsModified = true;

                dbcontext.SaveChanges();
                profile = FindProfile(request.Id).Profile;

                if (profile != null)
                {
                    response = new ProfileResponse
                    {
                        Profile = profile,
                        ResponseCode = "00",
                        ResponseDescription = "Location updated successfully"
                    };
                }
                else
                {
                    response = new ProfileResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "updated failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ProfileResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Update failed"
                };
            }
            Logger.LogInfo("ProfileService.UpdateProfilePicture", response);
            return Task.Factory.StartNew(() => response);
        }

        public Task<ProfileResponse> UpdateProfile(Profile Profile)
        {
            ProfileResponse response = null;
            try
            {
                _profileRepository.DetachAndUpdate(Profile.Id, Profile);
                //_profileRepository.Update( profile);
                response = FindProfile(Profile.Id);
            }
            catch (Exception ex)
            {
                response = new ProfileResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request"
                };
            }

            return Task.Factory.StartNew(() => response);
        }

        public ProfileLocation GetProfileDistance(decimal Latitude, decimal Longitude, string ChefID)
        {
            ProfileLocation response = new ProfileLocation();
            try
            {
                string Query = string.Format(System.Configuration.ConfigurationManager.AppSettings.Get("LocationQueryPerUser"), Latitude, Longitude, ChefID); //"23.012034" "72.510754"
                Logger.LogInfo("ChefService, RetrieveAllCategories Query", Query);
                using (ApplicationDbContext dbcontext = new ApplicationDbContext())
                {
                    var profilelocations = dbcontext.Database.SqlQuery<ProfileLocation>(Query);
                    response = profilelocations.FirstOrDefault();
                }
                return response;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return response;
            }
        }

        public Task<ProfileResponse> UpdateLocation(UpdateLocationRequest request)
        {
            Logger.LogInfo("ProfileService.UpdateLocation, Input", request);
            ProfileResponse response = null;
            Profile profile = new Profile
            {
                Id = request.ProfileID,
                Longitude = request.Longitude,
                Latitude = request.Latitude
            };

            try
            {
                dbcontext.Chef_Connect_Profiles.Attach(profile);
                dbcontext.Entry(profile).Property(x => x.Latitude).IsModified = true;
                dbcontext.Entry(profile).Property(x => x.Longitude).IsModified = true;
                dbcontext.SaveChanges();
                profile = FindProfile(request.ProfileID).Profile;
                if (profile != null)
                {
                    response = new ProfileResponse
                    {
                        Profile = profile,
                        ResponseCode = "00",
                        ResponseDescription = "Location updated successfully"
                    };
                }
                else
                {
                    response = new ProfileResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "updated failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ProfileResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Update failed"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<ProfileResponse> CreatePaymentToken(CreatePaymentToken request)
        {
            Logger.LogInfo("ProfileService.CreatePaymentToken, Input", request);

            ProfileResponse response = null;
            try
            {
                string customerToken = PaymentService.CreateCustomer(request.Token, request.Email);

                Logger.LogInfo("ProfileService.CreatePaymentToken, customerToken", customerToken);

                Profile profile = new Profile
                {
                    Id = request.ProfileID,
                    PaymentToken = customerToken,
                };


                dbcontext.Chef_Connect_Profiles.Attach(profile);
                dbcontext.Entry(profile).Property(x => x.PaymentToken).IsModified = true;
                dbcontext.SaveChanges();
                profile = FindProfile(request.ProfileID).Profile;
                if (profile != null)
                {
                    response = new ProfileResponse
                    {
                        Profile = profile,
                        ResponseCode = "00",
                        ResponseDescription = "Payment Token created successfully"
                    };
                }
                else
                {
                    response = new ProfileResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Creation failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ProfileResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = Logger.GetExceptionMessages(ex)
                };
            }
            Logger.LogInfo("ProfileService.CreatePaymentToken, Response", response);
            return Task.Factory.StartNew(() => response);
        }

        public Task<ProfileResponse> UpdatePaymentToken(UpdatePaymentToken request)
        {
            Logger.LogInfo("ProfileService.UpdatePaymentToken, Input", request);

            ProfileResponse response = null;
            Profile profile = null;
            try
            {
                profile = FindProfile(request.ProfileID).Profile;
                string CustomerID = PaymentService.UpdateCustomer(profile.PaymentToken, request.Token);

                if (CustomerID != string.Empty)
                {
                    response = new ProfileResponse
                    {
                        Profile = profile,
                        ResponseCode = "00",
                        ResponseDescription = "Payment Token updated successfully"
                    };
                }
                else
                {
                    response = new ProfileResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "updated failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ProfileResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to create payment token"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<ProfileResponse> UpdateFireBaseToken(UpdateFireBaseToken request)
        {
            Logger.LogInfo("ProfileService.UpdateFireBaseToken, Input", request);
            ProfileResponse response = null;
            Profile profile = new Profile
            {
                Id = request.ProfileID,
                FireBaseToken = request.Token,
            };

            try
            {
                dbcontext.Chef_Connect_Profiles.Attach(profile);
                dbcontext.Entry(profile).Property(x => x.FireBaseToken).IsModified = true;
                dbcontext.SaveChanges();
                profile = FindProfile(request.ProfileID).Profile;
                if (profile != null)
                {
                    response = new ProfileResponse
                    {
                        Profile = profile,
                        ResponseCode = "00",
                        ResponseDescription = "FireBase Token updated successfully"
                    };
                }
                else
                {
                    response = new ProfileResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "updated failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ProfileResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Update failed"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<ProfileResponse> UpdateTaxDetails(UpdateTaxDetails request)
        {
            Logger.LogInfo("ProfileService.UpdateTaxDetails, Input", request);
            ProfileResponse response = null;
            Profile profile = new Profile
            {
                Id = request.ProfileID,
                NationalInsurance = request.NationalInsurance,
                isSelfEmployed = request.isSelfEmployed,
                UTR = request.UTR,
            };

            try
            {
                dbcontext.Chef_Connect_Profiles.Attach(profile);
                dbcontext.Entry(profile).Property(x => x.NationalInsurance).IsModified = true;
                if (request.isSelfEmployed == true)
                    dbcontext.Entry(profile).Property(x => x.UTR).IsModified = true;
                dbcontext.Entry(profile).Property(x => x.isSelfEmployed).IsModified = true;
                dbcontext.SaveChanges();
                profile = FindProfile(request.ProfileID).Profile;
                if (profile != null)
                {
                    response = new ProfileResponse
                    {
                        Profile = profile,
                        ResponseCode = "00",
                        ResponseDescription = "Update Successful"
                    };
                }
                else
                {
                    response = new ProfileResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "updated failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ProfileResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Update failed"
                };
            }
            Logger.LogInfo("ProfileService.UpdateTaxDetails, Response", response);
            return Task.Factory.StartNew(() => response);
        }
    }
}
