﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Repositories;
using ChefConnect.Persistence;
using ChefConnect.Services.ServiceUtilities;
using ChefConnect.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheChefConnect.Services.ServiceEntites;
using ChefConnect.Services;
using TheChefConnect.Services.Extensions;
using TheChefConnect.Services.NotificationService;

namespace TheChefConnect.Services
{
    public class InvoiceService
    {
        private IInvoiceRepository _invoiceRepository;
        private ApplicationDbContext dbcontext = new ApplicationDbContext();
        private TimeZoneInfo britishZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

        private InvoiceService(IInvoiceRepository InvoiceRepository)
        {
            _invoiceRepository = InvoiceRepository;
        }

        public InvoiceService() : this(RepositoryFactory.GetInvoiceRepository())
        {
        }

        public InvoiceResponse AddSessionToInvoice(Session Session)
        {
            Logger.LogInfo("InvoiceService, AddSessionToInvoice Input", Session);

            Invoice invoice = _invoiceRepository.GetAll().Where(x => x.InvoiceStatus == PaymentStatus.PendingPayment && x.ChefId == Session.ChefId).ToList().LastOrDefault();
            InvoiceResponse response = null;
            try
            {
                if (invoice != null)
                {
                    invoice.ListOfDays = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<DateTime, List<Session>>>(invoice.ListOfDaysJson);
                    DateTime date = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);
                    List<Session> SessionsForTheDay = invoice.ListOfDays.Where(x => x.Key == date).FirstOrDefault().Value;

                    if (SessionsForTheDay != null)
                    {
                        SessionsForTheDay.Add(Session);
                        invoice.ListOfDays[date] = SessionsForTheDay;
                        invoice.ListOfDaysJson = Newtonsoft.Json.JsonConvert.SerializeObject(invoice.ListOfDays);
                    }
                    else
                    {
                        invoice.ListOfDays.Add(date, new List<Session>() { Session });
                        invoice.ListOfDaysJson = Newtonsoft.Json.JsonConvert.SerializeObject(invoice.ListOfDays);
                    }

                    invoice.RunningTotal += Session.TotalAmount;
                    invoice.HoursWorked += Session.HoursWorked;
                    dbcontext.Chef_Connect_Invoice.Attach(invoice);
                    dbcontext.Entry(invoice).Property(x => x.RunningTotal).IsModified = true;
                    dbcontext.Entry(invoice).Property(x => x.HoursWorked).IsModified = true;
                    dbcontext.Entry(invoice).Property(x => x.ListOfDaysJson).IsModified = true;

                }
                else
                {
                    Dictionary<DateTime, List<Session>> listOfDays = new Dictionary<DateTime, List<Session>>();
                    listOfDays.Add(DateTime.Now.Date, new List<Session> { Session });
                    invoice = new Invoice
                    {
                        ChefId = Session.ChefId,
                        DateOpened = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone),
                        DateClosed = null,
                        InvoiceStatus = PaymentStatus.PendingPayment,
                        HoursWorked = Session.HoursWorked,
                        RunningTotal = Session.TotalAmount,
                        InvoiceRef = string.Format("{0}", TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone).ToString("yyMMddhhmmssffff")),
                        ListOfDaysJson = Newtonsoft.Json.JsonConvert.SerializeObject(listOfDays)

                    };
                    _invoiceRepository.Save(invoice);
                }

                dbcontext.SaveChanges();


                response = new InvoiceResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL"
                };
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new InvoiceResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Filed to add session to invoice"
                };

            }

            Logger.LogInfo("InvoiceService.AddSessionToInvoice", response);
            return response;
        }

        public Task<GetInvoiceResponse> GetCurrentInvoice(GetInvoiceRequest request)
        {
            Logger.LogInfo("GetCurrentInvoice, Input", request);
            GetInvoiceResponse response = null;
            try
            {
                Invoice invoice = _invoiceRepository.GetAll().Where(x => x.InvoiceStatus == PaymentStatus.PendingPayment && x.ChefId == request.ChefID).ToList().LastOrDefault();
                if (invoice != null)
                {
                    invoice.ListOfDays = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<DateTime, List<Session>>>(invoice.ListOfDaysJson);

                    response = new GetInvoiceResponse
                    {
                        ChefInvoice = invoice,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new GetInvoiceResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No Invoice found for this chef"
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new GetInvoiceResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to fetch running invoice"
                };
            }

            Logger.LogInfo("GetCurrentInvoice, Output", response);
            return Task.Factory.StartNew(() => response);
        }

        public Task<InvoiceResponse> SendInvoiceToMail(GetInvoiceRequest request)
        {
            Logger.LogInfo("InvoiceService.SendInvoiceToMail, Input", request);
            string path = new SpreadSheetExt().WriteCurrentInvoiceToSheet(request.ChefID).Result;
            Chef chef = new ChefService().LoadFullDetails(request.ChefID).Chef;

            Task.Run(() => new EmailService().SendMail(new MailRequest { FilePath = path, MailSubject = "YOUR CHEF CONNECT RUNNING INVOICE", MailRecipient = chef.Profile.Email, MailBody = "Find Attached your running Invoice" }));

            return Task.Factory.StartNew(() => new InvoiceResponse { ResponseCode = "00", ResponseDescription = "SUCCESSFUL" });
        }
    }
}
