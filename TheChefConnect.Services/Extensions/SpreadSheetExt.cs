﻿using ChefConnect.Core.Entities;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheChefConnect.Services.ServiceEntites;

namespace TheChefConnect.Services.Extensions
{
    public class SpreadSheetExt
    {
        public Task<string> WriteCurrentInvoiceToSheet(int ChefID)
        {
            String path = string.Format(@"{0}\SpreadSheets\InvoiceSheet{1}.xlsx", AppDomain.CurrentDomain.BaseDirectory, DateTime.Now.ToString("ddMMyyyyhhmmss"));
            using (ExcelPackage package = new ExcelPackage())
            {
                Invoice invoice = new InvoiceService().GetCurrentInvoice(new GetInvoiceRequest { ChefID = ChefID }).Result.ChefInvoice;
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("testsheet");

                ws.Cells["B1"].Value = "Date";
                ws.Cells["C1"].Value = "Client Name";
                ws.Cells["D1"].Value = "Hours Worked";
                ws.Cells["E1"].Value = "Income";
                ws.Cells["B1:E1"].Style.Font.Bold = true;
                ws.Cells["B1:E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.LightTrellis;
                ws.Cells["B1:E1"].Style.Fill.BackgroundColor.SetColor(Color.SkyBlue);

                int x = 2;
                char date = 'B';
                char chef_Name = 'C';
                char hours_Worked = 'D';
                char income = 'E';

                foreach (var day in invoice.ListOfDays)
                {
                    foreach (var session in day.Value)
                    {
                        ws.Cells[chef_Name + x.ToString()].Value = session.DisplayName;
                        ws.Cells[hours_Worked + x.ToString()].Value = session.HoursWorked;
                        ws.Cells[income + x.ToString()].Value = session.TotalAmount;
                    }
                    x++;
                }
                x++;

                ws.Cells["A" + x.ToString()].Value = "TOTAL";
                ws.Cells[hours_Worked + x.ToString()].Value = invoice.HoursWorked;
                ws.Cells[income + x.ToString()].Value = invoice.RunningTotal;
                ws.Cells[string.Format("{0}:{1}:{2}", "A" + x.ToString(), hours_Worked + x.ToString(), income + x.ToString())].Style.Font.Bold = true;
                package.Save();

                //Create excel file on physical disk    
                using (FileStream objFileStrm = File.Create(path))
                {
                    objFileStrm.Close();

                    //Write content to excel file    
                    File.WriteAllBytes(path, package.GetAsByteArray());
                }
            }
            return Task.Factory.StartNew(() => path);
        }

        public Task<string> WrtiteOutstandingPaymentToSheet(int ProfileID)
        {
            String path = string.Format(@"{0}\SpreadSheets\PaymentSheet{1}.xlsx", AppDomain.CurrentDomain.BaseDirectory, DateTime.Now.ToString("ddMMyyyyhhmmss"));
            using (ExcelPackage package = new ExcelPackage())
            {
                OutstandignPayment invoice = new OutstandingPaymentService().GetCurrentOutstandingPayment(new GetOutstandignPaymentRequest { ProfileID = ProfileID }).Result.ClientOutstandingPayment;
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("testsheet");

                ws.Cells["B1"].Value = "Date";
                ws.Cells["C1"].Value = "Chef Name";
                ws.Cells["D1"].Value = "Hours Serviced";
                ws.Cells["E1"].Value = "Due Payment";
                ws.Cells["B1:E1"].Style.Font.Bold = true;
                ws.Cells["B1:E1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.LightTrellis;
                ws.Cells["B1:E1"].Style.Fill.BackgroundColor.SetColor(Color.SkyBlue);

                int x = 2;
                char date = 'B';
                char chef_Name = 'C';
                char hours_Worked = 'D';
                char income = 'E';

                foreach (var day in invoice.ListOfDays)
                {
                    foreach (var session in day.Value)
                    {
                        ws.Cells[chef_Name + x.ToString()].Value = session.DisplayName;
                        ws.Cells[hours_Worked + x.ToString()].Value = session.HoursWorked;
                        ws.Cells[income + x.ToString()].Value = session.TotalAmount;
                    }
                    x++;
                }
                x++;

                ws.Cells["A" + x.ToString()].Value = "TOTAL";
                ws.Cells[hours_Worked + x.ToString()].Value = invoice.HoursServiced;
                ws.Cells[income + x.ToString()].Value = invoice.RunningTotal;
                ws.Cells[string.Format("{0}:{1}:{2}", "A" + x.ToString(), hours_Worked + x.ToString(), income + x.ToString())].Style.Font.Bold = true;
                package.Save();

                //Create excel file on physical disk    
                using (FileStream objFileStrm = File.Create(path))
                {
                    objFileStrm.Close();

                    //Write content to excel file    
                    File.WriteAllBytes(path, package.GetAsByteArray());
                }
            }
            return Task.Factory.StartNew(() => path);
        }
    }
}
