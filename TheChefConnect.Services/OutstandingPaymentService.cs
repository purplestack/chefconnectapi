﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Entities.Common;
using ChefConnect.Core.Repositories;
using ChefConnect.Persistence;
using ChefConnect.Services;
using ChefConnect.Services.ServiceUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheChefConnect.Services.Extensions;
using TheChefConnect.Services.NotificationService;
using TheChefConnect.Services.ServiceEntites;

namespace TheChefConnect.Services
{
    public class OutstandingPaymentService
    {
        private IOutstandingPaymentRepository _outstandingPaymentRepository;
        private ApplicationDbContext dbContext = new ApplicationDbContext();
        private TimeZoneInfo britishZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

        private OutstandingPaymentService(IOutstandingPaymentRepository OutstandingPaymentRepository)
        {
            _outstandingPaymentRepository = OutstandingPaymentRepository;
        }

        public OutstandingPaymentService() : this(RepositoryFactory.GetOutstandingPaymentRepository())
        {

        }

        public OutstandingPaymentResponse AddSessionToOutstandingPayment(Session Session)
        {
            Logger.LogInfo("OutstandingPaymentService, Input", Session);

            OutstandignPayment outstandingPaymnet = _outstandingPaymentRepository.GetAll().Where(x => x.PaymentStatus == PaymentStatus.PendingPayment && x.ClientProfileID == Session.ChefId).ToList().LastOrDefault();
            OutstandingPaymentResponse response = null;
            try
            {
                if (outstandingPaymnet != null)
                {
                    outstandingPaymnet.ListOfDays = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<DateTime, List<Session>>>(outstandingPaymnet.ListOfDaysJson);
                    DateTime date = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone); ;
                    List<Session> SessionsForTheDay = outstandingPaymnet.ListOfDays.Where(x => x.Key == date).FirstOrDefault().Value;

                    if (SessionsForTheDay != null)
                    {
                        SessionsForTheDay.Add(Session);
                        outstandingPaymnet.ListOfDays[date] = SessionsForTheDay;
                        outstandingPaymnet.ListOfDaysJson = Newtonsoft.Json.JsonConvert.SerializeObject(outstandingPaymnet.ListOfDays);
                    }
                    else
                    {
                        outstandingPaymnet.ListOfDays.Add(date, new List<Session>() { Session });
                        outstandingPaymnet.ListOfDaysJson = Newtonsoft.Json.JsonConvert.SerializeObject(outstandingPaymnet.ListOfDays);
                    }

                    outstandingPaymnet.RunningTotal += Session.TotalAmount;
                    outstandingPaymnet.HoursServiced += Session.HoursWorked;
                    dbContext.Chef_Connect_OutstandingPaymnets.Attach(outstandingPaymnet);
                    dbContext.Entry(outstandingPaymnet).Property(x => x.RunningTotal).IsModified = true;
                    dbContext.Entry(outstandingPaymnet).Property(x => x.HoursServiced).IsModified = true;
                    dbContext.Entry(outstandingPaymnet).Property(x => x.ListOfDaysJson).IsModified = true;

                }
                else
                {
                    Dictionary<DateTime, List<Session>> listOfDays = new Dictionary<DateTime, List<Session>>();
                    listOfDays.Add(TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone), new List<Session> { Session });

                    outstandingPaymnet = new OutstandignPayment
                    {
                        ClientProfileID = Session.ClientProfileId,
                        DateOpened = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone),
                        DateClosed = null,
                        PaymentStatus = PaymentStatus.PendingPayment,
                        HoursServiced = Session.HoursWorked,
                        RunningTotal = Session.TotalAmount,
                        PaymentRef = string.Format("{0}", TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone).ToString("yyMMddhhmmssffff")),
                        ListOfDaysJson = Newtonsoft.Json.JsonConvert.SerializeObject(listOfDays)

                    };
                    _outstandingPaymentRepository.Save(outstandingPaymnet);
                }

                dbContext.SaveChanges();


                response = new OutstandingPaymentResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL"
                };
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new OutstandingPaymentResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Filed to add session to invoice"
                };

            }

            Logger.LogInfo("OutstandingPaymentService.AddSessionToOutstandingPayment", response);
            return response;
        }

        public Task<GetOutstandingPaymentResponse> GetCurrentOutstandingPayment(GetOutstandignPaymentRequest request)
        {
            Logger.LogInfo("GetCurrentOutstandingPayment, Input", request);
            GetOutstandingPaymentResponse response = null;
            try
            {
                OutstandignPayment outstandingPaymnet = _outstandingPaymentRepository.GetAll().Where(x => x.PaymentStatus == PaymentStatus.PendingPayment && x.ClientProfileID == request.ProfileID).ToList().LastOrDefault();
                if (outstandingPaymnet != null)
                {
                    outstandingPaymnet.ListOfDays = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<DateTime, List<Session>>>(outstandingPaymnet.ListOfDaysJson);

                    response = new GetOutstandingPaymentResponse
                    {
                        ClientOutstandingPayment = outstandingPaymnet,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new GetOutstandingPaymentResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No outstanding payment found for this profile"
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new GetOutstandingPaymentResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to fetch outstanding payment"
                };
            }

            Logger.LogInfo("GetCurrentOutstandingPayment, Output", response);
            return Task.Factory.StartNew(() => response);
        }

        public Task<OutstandingPaymentResponse> SendOutstandingPaymentToMail(GetOutstandignPaymentRequest request)
        {
            Logger.LogInfo("InvoiceService.SendInvoiceToMail, Input", request);
            string path = new SpreadSheetExt().WrtiteOutstandingPaymentToSheet(request.ProfileID).Result;
            Profile profile = new ProfileService().FindProfile(request.ProfileID).Profile;

            Task.Run(() => new EmailService().SendMail(new MailRequest { FilePath = path, MailSubject = "YOUR CHEF CONNECT OUTSTANDING PAYMENT", MailRecipient = profile.Email, MailBody = "Find Attached your outstanding payment" }));

            return Task.Factory.StartNew(() => new OutstandingPaymentResponse { ResponseCode = "00", ResponseDescription = "SUCCESSFUL" });
        }
    }
}
