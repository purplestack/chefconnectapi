﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TheChefConnect.Services.ServiceEntites;

namespace TheChefConnect.Services.NotificationService
{
    public class EmailService
    {
        public void SendMail(MailRequest request)
        {
            //MailRequest mailRequest
            using (MailMessage mail = new MailMessage("sosanbabajide@gmail.com", request.MailRecipient))
            {
                using (SmtpClient client = new SmtpClient())
                {
                    client.Port = 587;
                    client.Host = "smtp.gmail.com";
                    client.EnableSsl = true;
                    client.Timeout = 10000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    using (Attachment attachment = new Attachment(request.FilePath))
                    {
                        client.Credentials = new System.Net.NetworkCredential("sosanbabajide@gmail.com", "jide2014");
                        mail.Subject = request.MailSubject;
                        mail.Body = request.MailBody;
                        mail.Attachments.Add(attachment);

                        client.Send(mail);
                    }
                }
            }
            File.Delete(request.FilePath);
        }
    }
}
