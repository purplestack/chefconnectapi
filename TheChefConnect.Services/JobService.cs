﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Repositories;
using ChefConnect.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChefConnect.Services.ServiceEntites;
using ChefConnect.Services.ServiceUtilities;
using TheChefConnect.Services.ServiceEntites;
using TheChefConnect.Services.ServiceUtilities;
using ChefConnect.Core.Entities.Common;

namespace ChefConnect.Services
{
    public class JobService
    {
        private IJobRepository _jobRepository;
        private ApplicationDbContext dbcontext = new ApplicationDbContext();
        private JobService(IJobRepository JobRepository)
        {
            _jobRepository = JobRepository;
        }

        private TimeZoneInfo britishZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

        public JobService() : this(RepositoryFactory.GetJobRepository())
        {
        }

        public Task<JobResponse> RequestJob(Job job)
        {
            Logger.LogInfo("JobService,RequestJob: ", job);
            JobResponse response = null;
            try
            {
                job.StartTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);
                _jobRepository.Save(job);
                Chef chef = new ChefService().LoadFullDetails(job.ChefId).Chef;
                Profile client = new ProfileService().FindProfile(job.ProfileId).Profile;
                string Token = chef.Profile.FireBaseToken;

                FireBaseRequest request = new FireBaseRequest()
                {
                    to = Token,
                    data = new { messageType = "job", operation = "new", status = job.JobStatus, ProfileId = job.ProfileId, ChefId = job.ChefId, Id = job.Id },
                    notification = new Notification
                    {
                        title = "Chef Connect",
                        body = string.Format("New Job Request from {0}", client.DisplayName)
                    }
                };
                FireBaseResponse messageResponse = new FirebaseProcessor<FireBaseRequest, FireBaseResponse>().DoPOST(request) as FireBaseResponse;
                response = new JobResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL",
                    RequestedJob = job,
                };
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new JobResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to request job",
                    RequestedJob = job,
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<JobResponse> UpdateJobStatus(UpdateJobStatus request)
        {
            JobResponse response = null;
            try
            {
                Job job = new Job
                {
                    Id = request.Id,
                    JobStatus = request.JobStatus
                };
                dbcontext.Chef_Connect_Jobs.Attach(job);
                dbcontext.Entry(job).Property(x => x.JobStatus).IsModified = true;
                dbcontext.SaveChanges();
                response = FindJob(request.Id).Result;

                //fetch both chef and client profile for this job instance
                Profile client = new ProfileService().FindProfile(response.RequestedJob.ProfileId).Profile;
                Chef chef = new ChefService().LoadFullDetails(response.RequestedJob.ChefId).Chef;
                string Token = string.Empty;

                FireBaseRequest fireBaseRequest = new FireBaseRequest()
                {
                    to = Token,
                    data = new { messageType = "job", operation = "update", status = job.JobStatus, ProfileId = job.ProfileId, ChefId = job.ChefId, Id = job.Id },
                    notification = new Notification
                    {
                        title = "Chef Connect",
                    }
                };

                switch (request.JobStatus)
                {
                    case Core.Entities.Common.JobStatus.Approved:
                        Token = client.FireBaseToken;
                        fireBaseRequest.to = Token;
                        fireBaseRequest.notification.body = string.Format("Your job request to {0} has been apporved", chef.Profile.DisplayName);
                        break;
                    case Core.Entities.Common.JobStatus.Session_Started:
                        break;
                    case Core.Entities.Common.JobStatus.Declined:
                        break;
                    case Core.Entities.Common.JobStatus.Session_Completed:
                        break;
                    default:
                        break;
                }
                FireBaseResponse messageResponse = new FirebaseProcessor<FireBaseRequest, FireBaseResponse>().DoPOST(fireBaseRequest) as FireBaseResponse;


            }
            catch (Exception ex)
            {
                response = new JobResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request"
                };
            }

            return Task.Factory.StartNew(() => response);
        }

        public Task<JobResponse> FindJob(int Key)
        {
            Job job = new Job();
            JobResponse response = null;
            try
            {
                job = _jobRepository.Get(Key);

                response = new JobResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL",
                    RequestedJob = job,
                };
                if (job == null)
                {
                    response = new JobResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Unable to process request",
                        RequestedJob = job,
                    };
                }
            }
            catch (Exception ex)
            {
                response = new JobResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<GetAllJobsResponse> GetAllSentJobs(GetAllSentJob request)
        {
            Logger.LogInfo("JobService.GetAllSentJobs, Input", request);
            List<Job> jobs = new List<Job>();
            GetAllJobsResponse response = null;
            jobs = _jobRepository.GetAll().Where(x => x.ProfileId == request.ClientId).ToList();
            try
            {
                foreach (var job in jobs)
                {
                    Chef chef = new ChefService().FindChef(job.ChefId).Chef;
                    Profile profile = new ProfileService().FindProfile(chef.ProfileId).Profile;
                    job.Distance = new ProfileService().GetProfileDistance(profile.Latitude, profile.Longitude, job.ChefId.ToString()).DISTANCE;
                    job.DisplayName = profile.DisplayName;
                    job.Location = profile.Location;
                    job.Rate = chef.Rate;
                }

                if (jobs != null && jobs.Count() > 0)
                {
                    response = new GetAllJobsResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        Jobs = jobs,
                    };
                }
                else
                {
                    response = new GetAllJobsResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "No Jobs found",
                        Jobs = jobs,
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new GetAllJobsResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<GetAllJobsResponse> GetAllReceivedJobs(GetAllReceivedJob request)
        {
            List<Job> jobs = new List<Job>();
            GetAllJobsResponse response = null;
            jobs = _jobRepository.GetAll().Where(x => x.ChefId == request.ReceiverId).ToList();
            try
            {
                foreach (var job in jobs)
                {
                    Profile profile = new ProfileService().FindProfile(job.ProfileId).Profile;
                    Chef chef = new ChefService().LoadFullDetails(request.ReceiverId).Chef;
                    job.Distance = new ProfileService().GetProfileDistance(profile.Latitude, profile.Latitude, chef.Id.ToString()).DISTANCE;
                    job.DisplayName = profile.DisplayName;
                    job.Location = profile.Location;
                    job.Rate = chef.Rate;
                }

                if (jobs != null && jobs.Count() > 0)
                {
                    response = new GetAllJobsResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        Jobs = jobs,
                    };
                }
                else
                {
                    response = new GetAllJobsResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "No Jobs found",
                        Jobs = jobs,
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new GetAllJobsResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<GetJobHistoryResponse> GetJobHistory(GetJobHistory request)
        {
            List<Job> jobs = new List<Job>();
            PagedList<Job> pagedJobs = null;
            GetJobHistoryResponse response = null;
            jobs = _jobRepository.GetAll().Where(x => x.ChefId == request.ChefID || x.ProfileId == request.ClientId && (x.JobStatus == JobStatus.Session_Completed || x.JobStatus == JobStatus.Declined)).ToList();
            try
            {
                foreach (var job in jobs)
                {
                    Profile profile = new ProfileService().FindProfile(job.ProfileId).Profile;
                    Chef chef = new ChefService().LoadFullDetails(request.ChefID).Chef;
                    job.Distance = new ProfileService().GetProfileDistance(profile.Latitude, profile.Latitude, chef.Id.ToString()).DISTANCE;
                    job.DisplayName = profile.DisplayName;
                    job.Location = profile.Location;
                    job.Rate = chef.Rate;
                }

                if (jobs != null && jobs.Count() > 0)
                {
                    pagedJobs = new PagedList<Job>(jobs.AsQueryable().OrderBy(x => x.Id), request.CurrentPage, request.PageSize);
                    response = new GetJobHistoryResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        Jobs = pagedJobs,
                    };
                }
                else
                {
                    response = new GetJobHistoryResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "No Jobs found",
                        Jobs = pagedJobs,
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new GetJobHistoryResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request"
                };
            }
            return Task.Factory.StartNew(() => response);
        }
    }
}
