﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Repositories;
using ChefConnect.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChefConnect.Services.ServiceEntites;
using ChefConnect.Services.ServiceUtilities;
using System.Data.Entity;

namespace ChefConnect.Services
{
    public class ChefService
    {
        private double NatIns = 0.13;           //represents national Insurance
        private double PensContr = 0.03;        //represents Pension contribution
        private double HolPayContr = 0.05;      //represents Holiday Pay contribution
        private double Commission = 0.5;        // represents Commission
        private double GatewayCommiss = 0.0209; //represents payment gateway commission

        private IChefRepository _chefRepository;
        private ApplicationDbContext dbcontext = new ApplicationDbContext();
        private ChefService(IChefRepository ChefRepository)
        {
            _chefRepository = ChefRepository;
        }

        public ChefService() : this(RepositoryFactory.GetChefRepository())
        {
            double.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("NationalInsuranceContribution"), out NatIns);
            double.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("PensionContribution"), out PensContr);
            double.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("HolidayContribution"), out HolPayContr);
            double.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("Commission"), out Commission);
            double.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("PaymentGatewayCommission"), out GatewayCommiss);
        }

        public Task<ChefResponse> RegisterChef(Chef chef)
        {
            Logger.LogInfo("ChefService, RegisterChef", chef);
            ChefResponse response = null;
            try
            {
                Chef existingChef = dbcontext.Chef_Connect_Chefs.Include("Profile").Where(x => x.Profile.Email == chef.Profile.Email).FirstOrDefault();
                if (existingChef != null)
                {
                    response = new ChefResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Profile with this email already exists",
                        Chef = chef,
                    };
                }
                else
                {
                    _chefRepository.Save(chef);
                    response = new ChefResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        Chef = chef,
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ChefResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                    Chef = chef,
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public ChefListResponse RetrieveAll(GetAllChefsRequest request)
        {
            IQueryable<Chef> chefList = null;
            ChefListResponse response = null;
            List<Chef> ChefsFullDetails = new List<Chef>();
            try
            {
                chefList = _chefRepository.GetAll();
                if (chefList == null || chefList.Count() != 0)
                {
                    foreach (var item in chefList)
                    {
                        ChefsFullDetails.Add(LoadFullDetails(item.Id).Chef);
                    }
                    PagedList<Chef> pagedChef = new PagedList<Chef>(ChefsFullDetails.AsQueryable().OrderBy(x => x.Id), request.CurrentPage, request.PageSize);
                    response = new ChefListResponse
                    {
                        Chefs = pagedChef,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new ChefListResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No Chefs found"
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ChefListResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                    Chefs = null
                };
            }

            return response;
        }

        public ChefResponse FindChef(int Key)
        {
            Logger.LogInfo("ChefService.FindChef, Input", Key);
            Chef chef = new Chef();
            ChefResponse response = null;
            try
            {
                chef = _chefRepository.Get(Key);
                response = new ChefResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL",
                    Chef = chef,
                };
                if (chef == null)
                {
                    response = new ChefResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Chef not found",
                        Chef = chef,
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ChefResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                    Chef = chef,
                };
            }
            return response;
        }

        public ChefResponse LoadFullDetails(int key)
        {
            ChefResponse response = new ChefResponse
            {
                ResponseCode = "00",
                ResponseDescription = "SUCCESSFUL",
            };
            Chef chef = new Chef();

            try
            {
                chef = dbcontext.Chef_Connect_Chefs.Include("Profile").Where(x => x.Id == key).FirstOrDefault();
                response.Chef = chef;
            }
            catch (Exception ex)
            {
                response = new ChefResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = ex.Message,
                    Chef = chef,
                };
            }

            if (chef == null)
            {
                response.ResponseCode = "06";
                response.ResponseDescription = "Chef not found";
            }
            return response;
        }

        public Task<ChefResponse> UpdateChef(Chef chef)
        {
            ChefResponse response = null;
            try
            {
                _chefRepository.DetachAndUpdate(chef.Id, chef);
                if (chef.Profile != null)
                {
                    new ProfileService().UpdateProfile(chef.Profile);
                }
                response = FindChef(chef.Id);
            }
            catch (Exception ex)
            {
                response = new ChefResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                    Chef = chef,
                };
            }

            return Task.Factory.StartNew(() => response);
        }

        public Task<ChefResponse> GetChefByEmail(GetChefByEmail request)
        {
            ChefResponse response = null;
            Chef chef = null;
            try
            {
                chef = _chefRepository.GetAll().Where(x => x.Profile.Email == request.Email).FirstOrDefault();
                if (chef != null)
                {
                    response = new ChefResponse
                    {
                        Chef = chef,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new ChefResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No Chefs found"
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ChefResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                };
            }

            return Task.Factory.StartNew(() => response);
        }

        public Task<ChefListResponse> FindChefByUsername(GetChefByUsername request)
        {
            Logger.LogInfo("ChefService.FindChefByUserName", request);

            ChefListResponse response = null;
            IQueryable<Chef> chefs = null;
            try
            {
                chefs = dbcontext.Chef_Connect_Chefs.Include("Profile").Where(x => x.Profile.DisplayName.Contains(request.Username));

                if (chefs != null)
                {
                    PagedList<Chef> pagedChef = new PagedList<Chef>(chefs.AsQueryable().OrderBy(x => x.Id), request.CurrentPage, request.PageSize);
                    response = new ChefListResponse
                    {
                        Chefs = pagedChef,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new ChefListResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No Chefs found"
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ChefListResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                };
            }

            Logger.LogInfo("ChefService.Response", response);
            return Task.Factory.StartNew(() => response);
        }

        public Task<ChefListResponse> FindChefByKitchenExp(GetChefByKitchenExperience request)
        {
            Logger.LogInfo("ChefService.FindChefByKitchenExp", request);

            ChefListResponse response = null;
            IQueryable<Chef> chefs = null;
            try
            {

                string[] experienceType = request.KitchenExperiences.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string exp_One = string.Empty;
                string exp_Two = string.Empty;
                string exp_three = string.Empty;

                switch (experienceType.Count())
                {
                    case 1:
                        exp_One = experienceType[0];
                        chefs = dbcontext.Chef_Connect_Chefs.Include("Profile").Where(x => x.KitchenExperience.Contains(exp_One));
                        List<Chef> chek = chefs.ToList();
                        break;
                    case 2:
                        exp_One = experienceType[0];
                        exp_Two = experienceType[1];
                        chefs = dbcontext.Chef_Connect_Chefs.Include("Profile").Where(x => x.KitchenExperience.Contains(exp_One) && x.KitchenExperience.Contains(exp_Two));
                        break;
                    case 3:
                        exp_One = experienceType[0];
                        exp_Two = experienceType[1];
                        exp_three = experienceType[2];
                        chefs = dbcontext.Chef_Connect_Chefs.Include("Profile").Where(x => x.Profile.DisplayName.Contains(exp_One) && x.KitchenExperience.Contains(exp_Two) && x.KitchenExperience.Contains(exp_three));
                        break;
                    default:
                        break;
                }

                if (chefs != null)
                {
                    PagedList<Chef> pagedChef = new PagedList<Chef>(chefs.AsQueryable().OrderBy(x => x.Id), request.CurrentPage, request.PageSize);
                    response = new ChefListResponse
                    {
                        Chefs = pagedChef,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new ChefListResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No Chefs found"
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ChefListResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                };
            }

            Logger.LogInfo("ChefService.Response", response);
            return Task.Factory.StartNew(() => response);
        }

        public Task<ChefResponse> UpdateKitchenExperience(UpdateKitchenExperience request)
        {
            Logger.LogInfo("ChefService.UpdateKitchenExperience", request);

            ChefResponse response = new ChefResponse();
            try
            {
                Chef chef = new Chef
                {
                    Id = request.ID,
                    KitchenExperience = request.KitchenExperiences
                };

                dbcontext.Chef_Connect_Chefs.Attach(chef);
                dbcontext.Entry(chef).Property(x => x.KitchenExperience).IsModified = true;
                dbcontext.SaveChanges();
                chef = LoadFullDetails(request.ID).Chef;

                if (chef != null)
                {
                    response = new ChefResponse
                    {
                        Chef = chef,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new ChefResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ChefResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "An Error occured while updating chef kitchen experience"
                };
            }
            return Task.Factory.StartNew(() => response);

        }

        public Task<ChefLocationsResponse> GetChefLocations(string Latitude, string Longitude)
        {
            ChefLocationsResponse response = new ChefLocationsResponse();
            try
            {
                string Query = string.Format(System.Configuration.ConfigurationManager.AppSettings.Get("LocationQuery"), Latitude, Longitude); //"23.012034" "72.510754"
                Logger.LogInfo("ChefService, RetrieveAllCategories Query", Query);
                List<ChefLocation> locations = null;
                using (ApplicationDbContext dbcontext = new ApplicationDbContext())
                {
                    var cheflocations = dbcontext.Database.SqlQuery<ChefLocation>(Query);
                    locations = cheflocations.ToList();
                }
                return Task.Factory.StartNew(() => response = new ChefLocationsResponse
                {
                    ChefLocations = locations,
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL"
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return Task.Factory.StartNew(() => response = new ChefLocationsResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to complete request"
                });
            }
        }

        public ChefCategories RetrieveAllCategories(GetChefCategoriesRequest request)
        {
            Logger.LogInfo("ChefService, RetrieveAllCategories Input", request);
            List<Chef> chefList = null;
            ChefCategories response = null;
            List<Chef> ChefsFullDetails = new List<Chef>();
            List<Chef> ChefsLocations = new List<Chef>();
            Dictionary<string, PagedList<Chef>> chefCategory = new Dictionary<string, PagedList<Chef>>();
            try
            {
                chefList = _chefRepository.GetAll().ToList();
                if (chefList == null || chefList.Count() != 0)
                {
                    foreach (var item in chefList)
                    {
                        ChefsFullDetails.Add(LoadFullDetails(item.Id).Chef);
                    }

                    chefCategory.Add("Top Rated", new PagedList<Chef>(ChefsFullDetails.AsQueryable().OrderBy(x => x.Rating), request.CurrentPage, request.PageSize));

                    ChefLocationsResponse responseForLocation = GetChefLocations(request.Latitude, request.Longitude).Result;
                    foreach (var item in chefList)
                    {
                        Chef tempChef = chefList.Where(x => x.Id == item.Id).FirstOrDefault();
                        tempChef.Distance = responseForLocation.ChefLocations.Where(x => x.Id == item.Id).FirstOrDefault().DISTANCE;
                        ChefsLocations.Add(tempChef);
                    }
                    chefCategory.Add("ClosestToYou", new PagedList<Chef>(ChefsLocations.AsQueryable().OrderBy(x => x.Distance), request.CurrentPage, request.PageSize));

                    chefCategory.Add("Popular", new PagedList<Chef>(ChefsFullDetails.AsQueryable().OrderBy(x => x.Rating), request.CurrentPage, request.PageSize));

                    response = new ChefCategories
                    {
                        Categories = chefCategory,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new ChefCategories
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No Chefs found"
                    };
                }
            }
            catch (Exception ex)
            {
                response = new ChefCategories
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                };
            }
            Logger.LogInfo("ChefService, RetrieveAllCategories respons", response);
            return response;
        }

        public Task<ChefResponse> UpdateRate(UpdateRate request)
        {
            Logger.LogInfo("ProfileService.UpdateRate, Input", request);
            ChefResponse response = null;

            try
            {
                Chef validateChef = LoadFullDetails(request.ChefID).Chef;
                bool isselfEmployed = validateChef.Profile.isSelfEmployed;


                validateChef.Rate = (Decimal)GetTotalFee(Convert.ToDouble(request.Rate), false);
                validateChef.BaseRate = (Decimal)Convert.ToDouble(request.Rate);


                _chefRepository.Update(validateChef);

                validateChef = FindChef(request.ChefID).Chef;
                if (validateChef != null)
                {
                    response = new ChefResponse
                    {
                        Chef = validateChef,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new ChefResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ChefResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "An Error occured while updating rate"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<ChefResponse> UpdateBankDetails(UpdateBankDetails request)
        {
            Logger.LogInfo("ProfileService.UpdateBankDetails, Input", request);
            ChefResponse response = null;
            Chef chef = new Chef
            {
                Id = request.ChefID,
                AccountNumber = request.BankAccount,
                AccountName = request.BankAccountName,
                BankName = request.BankName
            };

            try
            {
                dbcontext.Chef_Connect_Chefs.Attach(chef);
                dbcontext.Entry(chef).Property(x => x.BankName).IsModified = true;
                dbcontext.Entry(chef).Property(x => x.AccountName).IsModified = true;
                dbcontext.Entry(chef).Property(x => x.AccountNumber).IsModified = true;
                dbcontext.SaveChanges();
                chef = FindChef(request.ChefID).Chef;
                if (chef != null)
                {
                    response = new ChefResponse
                    {
                        Chef = chef,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new ChefResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new ChefResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "An Error occured while updating Bank details"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public double GetTotalFee(double employeeRate, bool isSelfEmployed)
        {
            //Sets all the params to Zero for Self Emoplyed Chefs
            if (isSelfEmployed)
            {
                NatIns = 0;
                PensContr = 0;
                HolPayContr = 0;
            }

            // No Holiday pay for rates above 9.50
            if (employeeRate > 9.50)
            {
                HolPayContr = 0;
            }
            double y = ((1 + (NatIns + PensContr + HolPayContr)) * employeeRate) + Commission;   // 1st Order Estimate as in equation 

            return y * (Math.Pow(GatewayCommiss, 5) + (Math.Pow(2 * GatewayCommiss, 4)) + (Math.Pow(2 * GatewayCommiss, 3)) + (Math.Pow(2 * GatewayCommiss, 2)) + GatewayCommiss + 1);

        }
    }
}
