﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Repositories;
using ChefConnect.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChefConnect.Services.ServiceEntites;

namespace ChefConnect.Services
{
    public class UserService
    {
        private IUserRepository _userRepository;

        private UserService(IUserRepository UserRepository)
        {
            _userRepository = UserRepository;
        }

        public UserService() : this(RepositoryFactory.GetUserRepository())
        {
        }

        ApplicationDbContext dbContext = new ApplicationDbContext();

        public Task<UserResponse> RegisterUser(User user)
        {
            UserResponse response = null;
            try
            {
                User existinguser = dbContext.Chef_Connect_Users.Include("Profile").Where(x => x.Profile.Email == user.Profile.Email).FirstOrDefault();
                if (existinguser != null)
                {
                    response = new UserResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Profile with this email already exists",
                        User = user,
                    };
                }
                else
                {
                    _userRepository.Save(user);
                    response = new UserResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        User = user,
                    };
                }
            }
            catch (Exception ex)
            {
                response = new UserResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                    User = user,
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public UserListResponse RetrieveAll(UserRequest request)
        {
            IQueryable<User> userList = null;
            UserListResponse response = null;
            try
            {
                userList = _userRepository.GetAll();
                if (userList == null || userList.Count() != 0)
                {

                    PagedList<User> pagedUsers = new PagedList<User>(userList.OrderBy(x => x.Id), request.CurrentPage, request.PageSize);
                    response = new UserListResponse
                    {
                        Users = pagedUsers,
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL"
                    };
                }
                else
                {
                    response = new UserListResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No Chefs found"
                    };
                }
            }
            catch (Exception ex)
            {
                response = new UserListResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                    Users = null
                };
            }

            return response;
        }

        public UserResponse FindUser(int Key)
        {
            User user = new User();
            UserResponse response = null;
            try
            {
                user = _userRepository.Get(Key);
                if (user == null)
                {
                    response = new UserResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "User not found"
                    };
                }
                else
                {
                    response = new UserResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        User = user
                    };
                }


            }
            catch (Exception ex)
            {
                response = new UserResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = " Unable to find user",
                    User = user,
                };
            }
            return response;
        }

        public UserResponse LoadFullDetails(int key)
        {
            UserResponse response = new UserResponse
            {
                ResponseCode = "00",
                ResponseDescription = "SUCCESSFUL",
            };
            User user = new User();

            try
            {
                user = dbContext.Chef_Connect_Users.Include("Profile").Where(x => x.Id == key).FirstOrDefault();
                response.User = user;
            }
            catch (Exception ex)
            {
                response = new UserResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = ex.Message,
                    User = user,
                };
            }

            if (user == null)
            {
                response.ResponseCode = "06";
                response.ResponseDescription = "User not found";
            }
            return response;
        }

        public Task<UserResponse> UpdateUser(User user)
        {
            UserResponse response = null;
            try
            {
                _userRepository.DetachAndUpdate(user.Id, user);
                if (user.Profile != null)
                {
                    new ProfileService().UpdateProfile(user.Profile);
                }
                response = FindUser(user.Id);
            }
            catch (Exception ex)
            {
                response = new UserResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = ex.Message,
                    User = user,
                };
            }

            return Task.Factory.StartNew(() => response);
        }
    }
}
