﻿using ChefConnect.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Services.ServiceEntites
{
    public class FindChefByID
    {
        public int ID { get; set; }
    }

    public class GetAllChefsRequest : BaseRequest { }

    public class GetChefCategoriesRequest : BaseRequest
    {
        public string Longitude { get; set; }
        public string Latitude { get; set; }
    }

    public class GetChefByEmail
    {
        public string Email { get; set; }
    }

    public class GetChefByUsername : BaseRequest
    {
        public string Username { get; set; }
    }

    public class GetChefByKitchenExperience : BaseRequest
    {
        public string KitchenExperiences { get; set; }
    }

    public class UpdateKitchenExperience
    {
        public int ID { get; set; }
        public string KitchenExperiences { get; set; }
    }

    public class ChefResponse : BaseResponse
    {
        public Chef Chef { get; set; }
    }

    public class ChefLocationsResponse : BaseResponse
    {
        public List<ChefLocation> ChefLocations { get; set; }
    }

    public class ChefLocation
    {
        public int Id { get; set; }
        public double DISTANCE { get; set; }
    }

    public class ChefListResponse : BaseResponse
    {
        public PagedList<Chef> Chefs { get; set; }
    }

    public class ChefCategories : BaseResponse
    {
        public Dictionary<string, PagedList<Chef>> Categories { get; set; }
    }
}
