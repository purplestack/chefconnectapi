﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Services.ServiceEntites
{
    public abstract class BaseRequest
    {
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
    }
}
