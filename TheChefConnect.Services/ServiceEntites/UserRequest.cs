﻿using ChefConnect.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Services.ServiceEntites
{
    public class UserRequest:BaseRequest
    {
        public User User { get; set; }
    }

    public class UserResponse : BaseResponse
    {
        public User User { get; set; }
    }

    public class UserListResponse : BaseResponse
    {
        public PagedList<User> Users { get; set; }
    }
}
