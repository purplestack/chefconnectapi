﻿using ChefConnect.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Services.ServiceEntites
{
    public class LoginRequest
    {
        public string emailAddress { get; set; }
        public string Password { get; set; }
    }

    public class ForgotPasswordRequest
    {
        public string Email { get; set; }
    }

    public class ResetPasswordRequest
    {
        public string Email { get; set; }
        public string OTP { get; set; }
        public string NewPassword { get; set; }
    }
    public class ChangePasswordRequest
    {
        public int Id { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class LoginResponse : BaseResponse
    {
        public ProfileType ProfileType { get; set; }
        public Chef chef { get; set; }
        public User user { get; set; }
    }

    public class ProfilePictureRequest
    {
        public int Id { get; set; }
    }

    public class UpdatePictureRequest
    {
        public int Id { get; set; }
        public string ProfilePicture { get; set; }
    }

    public class UpdateProfileRequest
    {
        public Profile Profile { get; set; }
    }

    public class ProfileResponse : BaseResponse
    {
        public Profile Profile { get; set; }
    }

    public class ProfilePictureResponse : BaseResponse
    {
        public string ProfilePicture { get; set; }
    }

    public class ProfileLocation
    {
        public int Id { get; set; }
        public double DISTANCE { get; set; }
    }

    public class UpdateLocationRequest
    {
        public int ProfileID { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
    }

    public class CreatePaymentToken
    {
        public int ProfileID { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }
    }

    public class UpdatePaymentToken
    {
        public int ProfileID { get; set; }
        public string Token { get; set; }
    }

    public class UpdateTaxDetails
    {
        public int ProfileID { get; set; }
        public string UTR { get; set; }
        public string NationalInsurance { get; set; }
        public bool isSelfEmployed { get; set; }
    }

    public class UpdateFireBaseToken
    {
        public int ProfileID { get; set; }
        public string Token { get; set; }
    }

    public class UpdateRate
    {
        public int ChefID { get; set; }
        public string Rate { get; set; }
    }

    public class UpdateBankDetails
    {
        public int ChefID { get; set; }
        public string BankName { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccount { get; set; }
    }

    public class UpdateFavourites
    {
        public string ProfileID { get; set; }
        public List<int> Favourites { get; set; }
    }
}
