﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Entities.Common;
using ChefConnect.Services.ServiceEntites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheChefConnect.Services.ServiceEntites
{
    public class SessionResponse : BaseResponse
    {
        public Session Session { get; set; }
    }

    public class AllSessionResponse : BaseResponse
    {
        public List<Session> Sessions { get; set; }
    }

    public class SessionRequest
    {
        public int SessionId { get; set; }

    }

    public class SessionByJobRequest
    {
        public int JobId { get; set; }
    }

    public class UpdateSessionRequest
    {
        public int SessionId { get; set; }
        public SessionStatus SessionStatus { get; set; }
    }

    public class UpdateSessionTimeRequest
    {
        public int SessionId { get; set; }
        public string NewTime { get; set; }
    }

    public class ClockSessionRequest
    {
        public int SessionId { get; set; }
        public ClockType ClockType { get; set; }
        public string Review { get; set; }
        public int Rating { get; set; }
        public int RequestSenderID { get; set; }
    }

    public class CurrentSessionRequest
    {
        public int JobID { get; set; }
    }

    public class GetRequestedSession
    {
        public int ClientProfileID { get; set; }
    }

    public class GetReceivedSession
    {
        public int ChefID { get; set; }
    }

    public class CurrentSessions : BaseResponse
    {
        public List<Session> CurrentSession { get; set; }
    }

    public class UpdateSessionRating
    {
        public int SessionID { get; set; }
        public int Rating { get; set; }
        public string Review { get; set; }
    }

    public enum ClockType
    {
        Start = 0,
        End = 1
    }
}
