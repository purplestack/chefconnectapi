﻿using ChefConnect.Core.Entities;
using ChefConnect.Services;
using ChefConnect.Services.ServiceEntites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheChefConnect.Services.ServiceEntites
{
    public class PaymentResponse : BaseResponse
    {
        public Payment Payment { get; set; }
    }

    public class PaymentHistory : BaseResponse
    {
        public PagedList<Payment> Payments { get; set; }
    }

    public class GetPaymentHistory : BaseRequest
    {
        public int ChefID { get; set; }
    }
    public class GetPaymentByJobID : BaseRequest
    {
        public int JobID { get; set; }
    }

    public class GetInvoiceHistory : BaseRequest
    {
        public int ClientID { get; set; }
    }

    public class CustomerCard : BaseResponse
    {
        public string CardScheme { get; set; }
        public string LastFourDigit { get; set; }
        public string Country { get; set; }
    }
}
