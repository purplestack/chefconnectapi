﻿using ChefConnect.Core.Entities;
using ChefConnect.Services.ServiceEntites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheChefConnect.Services.ServiceEntites
{
    public class InvoiceResponse : BaseResponse
    {
    }

    public class GetInvoiceResponse : BaseResponse
    {
        public Invoice ChefInvoice { get; set; }
    }

    public class GetInvoiceRequest
    {
        public int ChefID { get; set; }
    }
   

}
