﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheChefConnect.Services.ServiceEntites
{
    public class FireBaseRequest
    {
        public string to { get; set; }
        public Notification notification { get; set; }
        public object data { get; set; }
    }

    public class Notification
    {
        public string title { get; set; }
        public string body { get; set; }
    }

    public class Data
    {
        public string name { get; set; }
        public string age { get; set; }
    }
}
