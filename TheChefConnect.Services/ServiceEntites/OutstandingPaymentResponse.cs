﻿using ChefConnect.Core.Entities;
using ChefConnect.Services.ServiceEntites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheChefConnect.Services.ServiceEntites
{
    public class OutstandingPaymentResponse : BaseResponse
    {
    }

    public class GetOutstandingPaymentResponse : BaseResponse
    {
        public OutstandignPayment ClientOutstandingPayment { get; set; }
    }

    public class GetOutstandignPaymentRequest
    {
        public int ProfileID { get; set; }
    }
}
