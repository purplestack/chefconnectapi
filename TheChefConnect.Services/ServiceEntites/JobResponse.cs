﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChefConnect.Services.ServiceEntites
{
    public class JobResponse : BaseResponse
    {
        public Job RequestedJob { get; set; }
    }

    public class GetAllJobsResponse : BaseResponse
    {
        public List<Job> Jobs { get; set; }
    }

    public class GetJobHistoryResponse : BaseResponse
    {
        public PagedList<Job> Jobs { get; set; }
    }

    public class UpdateJobStatus
    {
        public int Id { get; set; }
        public JobStatus JobStatus { get; set; }
    }

    public class UpdateJobResponse : BaseResponse
    {
        public Job UpdatedJob { get; set; }
    }

    public class JobRequest
    {
        public int Id { get; set; }
    }

    public class GetAllReceivedJob
    {
        public int ReceiverId { get; set; }
        public int SenderID { get; }
        public ProfileType ProfileType { get; set; }
    }

    public class GetJobHistory : BaseRequest
    {
        public int ClientId { get; set; }
        public int ChefID { get; set; }
    }

    public class GetAllSentJob
    {
        public int ClientId { get; set; }
    }
}
