﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheChefConnect.Services.ServiceEntites
{
    public class MailRequest
    {
        public string MailBody { get; set; }
        public string MailRecipient { get; set; }
        public string MailSubject { get; set; }
        public string FilePath { get; set; }
    }
}
