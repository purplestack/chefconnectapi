﻿using ChefConnect.Core.Entities;
using ChefConnect.Core.Entities.Common;
using ChefConnect.Core.Repositories;
using ChefConnect.Persistence;
using ChefConnect.Services;
using ChefConnect.Services.ServiceEntites;
using ChefConnect.Services.ServiceUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheChefConnect.Services.ServiceEntites;
using TheChefConnect.Services.ServiceUtilities;

namespace TheChefConnect.Services
{
    public class SessionService
    {
        private ISessionRepository _userRepository;
        private ApplicationDbContext dbcontext = new ApplicationDbContext();
        private double _configuredTax = double.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("ChefConnectTax"));
        private double _configuredCommission = double.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("Commission"));
        //private decimal FlatFee = 0.20M;
        private TimeZoneInfo britishZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

        private SessionService(ISessionRepository UserRepository)
        {
            _userRepository = UserRepository;
        }

        public SessionService() : this(RepositoryFactory.GetSessionRepository())
        {
        }

        public Task<SessionResponse> CreateSession(Session request)
        {
            Logger.LogInfo("SessionService.CreateSession, Input: ", request);
            SessionResponse response = null;
            try
            {
                request.Date = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);
                request.SessionStatus = SessionStatus.Ongoing;
                request.TimeIn = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);
                _userRepository.Save(request);
                Session session = FindSession(request.Id).Result.Session;

                //Make firebase call
                Profile client = new ProfileService().FindProfile(session.ClientProfileId).Profile;
                Chef chef = new ChefService().LoadFullDetails(session.ChefId).Chef;
                string Token = client.FireBaseToken;

                if (session != null)
                {
                    response = new SessionResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        Session = request,
                    };
                }
                else
                {
                    response = new SessionResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Failed to create session",
                        Session = request,
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new SessionResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request",
                };
            }
            Logger.LogInfo("SessionService.CreateSession, response: ", response);
            return Task.Factory.StartNew(() => response);
        }

        public Task<SessionResponse> ClockSession(ClockSessionRequest request)
        {
            SessionResponse response = null;
            UpdateJobStatus updateJobStatus = null;
            try
            {
                Session session = new Session();
                session = FindSession(request.SessionId).Result.Session;

                switch (request.ClockType)
                {
                    case ClockType.Start:
                        session.SessionStatus = SessionStatus.Ongoing;
                        session.TimeIn = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);
                        
                        break;
                    case ClockType.End:
                        session.SessionStatus = SessionStatus.Ended;
                        session.TimeOut = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);
                        session.Rating = request.Rating;
                        session.Review = request.Review;
                        break;
                    default:
                        break;
                }
                _userRepository.Update(session);
                session = FindSession(request.SessionId).Result.Session;

                //Make firebase call
                Profile client = new ProfileService().FindProfile(session.ClientProfileId).Profile;
                Chef chef = new ChefService().LoadFullDetails(session.ChefId).Chef;
                string Token = client.FireBaseToken;

                TimeSpan duration = session.TimeOut.Subtract(session.TimeIn);
                decimal amount = (Convert.ToDecimal(duration.TotalMinutes) * Convert.ToDecimal(chef.Rate)) / 60;
                decimal amountAfterTax = (Convert.ToDecimal(duration.TotalMinutes) * Convert.ToDecimal(chef.BaseRate)) / 60;
                //amount = amount + FlatFee;

                #region Update Session hours worked and Total fee

                session.HoursWorked = Convert.ToDecimal(duration.TotalHours);
                session.TotalAmount = amount;
                dbcontext.Entry(session).Property(x => x.HoursWorked).IsModified = true;
                dbcontext.Entry(session).Property(x => x.TotalAmount).IsModified = true;
                dbcontext.SaveChanges();
                #endregion

                FireBaseRequest fireBaseRequest = new FireBaseRequest()
                {
                    to = Token,
                    data = new { messageType = "session", operation = "update", status = session.SessionStatus, ProfileId = session.ClientProfileId, ChefId = session.ChefId, Id = session.Id },
                    notification = new Notification
                    {
                        title = "Chef Connect",
                    }
                };

                fireBaseRequest.to = Token;

                switch (request.ClockType)
                {
                    case ClockType.Start:
                        fireBaseRequest.notification.body = string.Format("Your session with {0} has started", chef.Profile.DisplayName);

                        // update job status
                        updateJobStatus = new UpdateJobStatus
                        {
                            Id = session.JobId,
                            JobStatus = JobStatus.Session_Started
                        };
                        new JobService().UpdateJobStatus(updateJobStatus);
                        break;
                    case ClockType.End:
                        if (request.RequestSenderID != 0)
                        {
                            // Checking if the requester is a user
                            if (request.RequestSenderID == session.ClientProfileId)
                            {
                                fireBaseRequest.to = chef.Profile.FireBaseToken;
                                fireBaseRequest.notification.body = string.Format("Your session with {0} has Ended", client.DisplayName);
                            }

                        }
                        else
                        {
                            fireBaseRequest.notification.body = string.Format("Your session with {0} has Ended", chef.Profile.DisplayName);
                        }

                        // update job status
                        updateJobStatus = new UpdateJobStatus
                        {
                            Id = session.JobId,
                            JobStatus = JobStatus.Session_Completed
                        };
                        new JobService().UpdateJobStatus(updateJobStatus);

                        //Add session to outstanding payment
                        //Modification to include chef name in sessino entity
                        session.DisplayName = chef.Profile.DisplayName;
                        new OutstandingPaymentService().AddSessionToOutstandingPayment(session);

                        //Creating session in invoice
                        //Modification to incluide display name in the session entity
                        session.TotalAmount = amountAfterTax;
                        session.DisplayName = new ProfileService().FindProfile(session.ClientProfileId).Profile.DisplayName;
                        new InvoiceService().AddSessionToInvoice(session);



                        #region Payment process
                        //string errorMsg = string.Empty;
                        //string PaymentID = new PaymentService().ChargeCustomer(client.PaymentToken, amount * 100, out errorMsg);
                        //if (PaymentID != null)
                        //{
                        //    payment.StripeChargeID = PaymentID;
                        //    payment.ErrorMsg = errorMsg;
                        //}
                        //else
                        //{
                        //    payment.ErrorMsg = errorMsg;
                        //}

                        //new PaymentService().CreatePayment(payment); 
                        #endregion
                        break;
                    default:
                        break;
                }



                FireBaseResponse messageResponse = new FirebaseProcessor<FireBaseRequest, FireBaseResponse>().DoPOST(fireBaseRequest) as FireBaseResponse;

                if (session != null)
                {
                    response = new SessionResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        Session = session,
                    };
                }
                else
                {
                    response = new SessionResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Failed to update session",
                        Session = session,
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new SessionResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request",
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<SessionResponse> UpdateSessionStatus(UpdateSessionRequest request)
        {
            SessionResponse response = null;
            try
            {
                Session session = new Session
                {
                    Id = request.SessionId,
                    SessionStatus = request.SessionStatus
                };

                dbcontext.Chef_Connect_Session.Attach(session);
                dbcontext.Entry(session).Property(x => x.SessionStatus).IsModified = true;
                //_profileRepository.Update( profile);
                dbcontext.SaveChanges();

                session = FindSession(request.SessionId).Result.Session;

                //Make firebase call
                Profile client = new ProfileService().FindProfile(session.ClientProfileId).Profile;
                Chef chef = new ChefService().LoadFullDetails(session.ChefId).Chef;
                string Token = client.FireBaseToken;

                FireBaseRequest fireBaseRequest = new FireBaseRequest()
                {
                    data = new { messageType = "session", operation = "update", status = session.SessionStatus, ProfileId = session.ClientProfileId, ChefId = session.ChefId, Id = session.Id },
                    notification = new Notification
                    {
                        title = "Chef Connect",
                        body = string.Format("A session has been initiated by {0} ", chef.Profile.DisplayName),
                    }
                };

                switch (request.SessionStatus)
                {
                    case SessionStatus.Canceled:
                        Token = chef.Profile.FireBaseToken;
                        fireBaseRequest.to = Token;
                        fireBaseRequest.notification.body = string.Format("your job was cancelled by {0}", client.DisplayName);
                        new JobService().UpdateJobStatus(new UpdateJobStatus { Id = session.JobId, JobStatus = JobStatus.Cancelled });
                        break;
                    case SessionStatus.Confirmed:
                        Token = chef.Profile.FireBaseToken;
                        fireBaseRequest.to = Token;
                        fireBaseRequest.notification.body = string.Format("your session has been confirmed by {0} and resumed at {1}", client.DisplayName, TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone).ToString());
                        break;
                    case SessionStatus.Ended:
                        Token = client.FireBaseToken;
                        fireBaseRequest.to = Token;
                        fireBaseRequest.notification.body = string.Format("your session has ended at {1} by {0} ", chef.Profile.DisplayName, TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone).ToString());
                        break;
                    default:
                        break;
                }

                FireBaseResponse messageResponse = new FirebaseProcessor<FireBaseRequest, FireBaseResponse>().DoPOST(fireBaseRequest) as FireBaseResponse;

                response = new SessionResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL",
                    Session = session
                };
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new SessionResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request"
                };
            }

            return Task.Factory.StartNew(() => response);
        }

        public Task<SessionResponse> FindSession(int Key)
        {
            Session session = new Session();
            SessionResponse response = null;
            try
            {
                dbcontext = new ApplicationDbContext();
                session = dbcontext.Chef_Connect_Session.Include("Job").Where(x => x.Id == Key).FirstOrDefault();

                response = new SessionResponse
                {
                    ResponseCode = "00",
                    ResponseDescription = "SUCCESSFUL",
                    Session = session,
                };
                if (session == null)
                {
                    response = new SessionResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "No Session found",
                        Session = session,
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new SessionResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<AllSessionResponse> GetAllSession(int Id)
        {
            List<Session> sessions = new List<Session>();
            AllSessionResponse response = null;
            try
            {
                sessions = _userRepository.GetAll().Where(x => x.JobId == Id).ToList();

                if (sessions == null && sessions.Count() == 0)
                {
                    response = new AllSessionResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Unable to process request",
                        Sessions = sessions
                    };
                }
                else
                {
                    response = new AllSessionResponse
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        Sessions = sessions
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new AllSessionResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Unable to process request"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<CurrentSessions> GetCurrentSessions(CurrentSessionRequest request)
        {
            Logger.LogInfo("SessionService.GetCurrentSessions", request);
            CurrentSessions response = null;
            try
            {
                List<Session> sessions = dbcontext.Chef_Connect_Session.Where(x => x.JobId == request.JobID || x.SessionStatus == SessionStatus.Ongoing).ToList();
                if (sessions != null)
                {
                    response = new CurrentSessions
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        CurrentSession = sessions,
                    };
                }
                else
                {
                    response = new CurrentSessions
                    {
                        ResponseCode = "06",
                        ResponseDescription = "There are no current sessions for this user",
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new CurrentSessions
                {
                    ResponseCode = "06",
                    ResponseDescription = "There are no current sessions for this user",
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<CurrentSessions> GetRequestedSessions(GetRequestedSession request)
        {
            Logger.LogInfo("SessionService.GetRequestedSessinos", request);
            CurrentSessions response = null;
            try
            {
                List<Session> sessions = dbcontext.Chef_Connect_Session.Where(x => (x.ClientProfileId == request.ClientProfileID) && ((x.SessionStatus == SessionStatus.Ongoing) || (x.SessionStatus == SessionStatus.Pending) || (x.SessionStatus == SessionStatus.Confirmed))).ToList();
                foreach (var session in sessions)
                {

                    int Id = new ChefService().FindChef(session.ChefId).Chef.ProfileId;
                    string name = new ProfileService().FindProfile(Id).Profile.DisplayName;
                    session.DisplayName = name;
                }
                if (sessions != null)
                {
                    response = new CurrentSessions
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        CurrentSession = sessions
                    };
                }
                else
                {
                    response = new CurrentSessions
                    {
                        ResponseCode = "06",
                        ResponseDescription = "There are no current requested sessions for this user",
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new CurrentSessions
                {
                    ResponseCode = "06",
                    ResponseDescription = "There are no current requested sessions for this client",
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<CurrentSessions> GetReceivedSessions(GetReceivedSession request)
        {
            Logger.LogInfo("SessionService.GetRequestedSessinos", request);
            CurrentSessions response = null;
            try
            {
                List<Session> sessions = dbcontext.Chef_Connect_Session.Where(x => x.ChefId == request.ChefID && (x.SessionStatus == SessionStatus.Ongoing || x.SessionStatus == SessionStatus.Pending || x.SessionStatus == SessionStatus.Confirmed)).ToList();
                foreach (var session in sessions)
                {
                    string name = new ProfileService().FindProfile(session.ClientProfileId).Profile.DisplayName;
                    session.DisplayName = name;
                }
                if (sessions != null)
                {
                    response = new CurrentSessions
                    {
                        ResponseCode = "00",
                        ResponseDescription = "SUCCESSFUL",
                        CurrentSession = sessions,
                    };
                }
                else
                {
                    response = new CurrentSessions
                    {
                        ResponseCode = "06",
                        ResponseDescription = "There are no current requested sessions for this user",
                    };
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new CurrentSessions
                {
                    ResponseCode = "06",
                    ResponseDescription = "There are no current requested sessions for this client",
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<SessionResponse> UpdateRatingAndReview(UpdateSessionRating Request)
        {
            Logger.LogInfo("ProfileService.UpdateLocation, Input", Request);
            SessionResponse response = null;
            Session session = new Session
            {
                Id = Request.SessionID,
                Rating = Request.Rating,
                Review = Request.Review
            };

            try
            {
                dbcontext.Chef_Connect_Session.Attach(session);
                dbcontext.Entry(session).Property(x => x.Rating).IsModified = true;
                dbcontext.Entry(session).Property(x => x.Review).IsModified = true;
                dbcontext.SaveChanges();
                session = FindSession(Request.SessionID).Result.Session;
                if (session != null)
                {
                    response = new SessionResponse
                    {
                        Session = session,
                        ResponseCode = "00",
                        ResponseDescription = "Rating updated successfully"
                    };
                }
                else
                {
                    response = new SessionResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "updated failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new SessionResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Update failed"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

        public Task<SessionResponse> UpdateStartTime(UpdateSessionTimeRequest Request)
        {
            Logger.LogInfo("ProfileService.UpdateStartTime, Input", Request);
            SessionResponse response = null;
            DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);
            try
            {
                currentTime = DateTime.Parse(Request.NewTime);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new SessionResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Invalid time"
                };
                return Task.Factory.StartNew(() => response);
            }
            Session session = new Session
            {
                Id = Request.SessionId,
                TimeIn = currentTime,
                StartTimeModified = true
            };

            try
            {
                if (session.TimeIn > TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone))
                {
                    return Task.Factory.StartNew(() => new SessionResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "Invalid start time"
                    });
                }

                dbcontext.Chef_Connect_Session.Attach(session);
                dbcontext.Entry(session).Property(x => x.TimeIn).IsModified = true;
                dbcontext.Entry(session).Property(x => x.StartTimeModified).IsModified = true;
                dbcontext.SaveChanges();


                session = FindSession(Request.SessionId).Result.Session;

                Chef chef = new ChefService().LoadFullDetails(session.ChefId).Chef;
                Profile client = new ProfileService().FindProfile(session.ClientProfileId).Profile;
                string Token = chef.Profile.FireBaseToken;

                if (session != null)
                {
                    string message = string.Format("Your Shift start time with {0} has been updated to {1}", client.DisplayName, session.TimeIn);
                    FireBaseRequest fireBaseRequest = new FireBaseRequest()
                    {
                        to = Token,
                        notification = new Notification
                        {
                            title = "Chef Connect",
                            body = message
                        },
                        data = new
                        {
                            messageType = "session",
                            operation = "updateTime",
                            status = session.SessionStatus,
                            ProfileId = session.ClientProfileId,
                            ChefId = session.ChefId,
                            Id = session.Id,
                            DisplayMessage = message
                        },
                    };

                    FireBaseResponse messageResponse = new FirebaseProcessor<FireBaseRequest, FireBaseResponse>().DoPOST(fireBaseRequest) as FireBaseResponse;

                    response = new SessionResponse
                    {
                        Session = session,
                        ResponseCode = "00",
                        ResponseDescription = "Time updated successfully"
                    };
                }
                else
                {
                    response = new SessionResponse
                    {
                        ResponseCode = "06",
                        ResponseDescription = "updated failed"
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                response = new SessionResponse
                {
                    ResponseCode = "06",
                    ResponseDescription = "Update failed"
                };
            }
            return Task.Factory.StartNew(() => response);
        }

    }
}
